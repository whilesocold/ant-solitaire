var OptionsDialog = CDialog.extend({
    preInit: function(props) {
        this._super(props);

        this.playShowEffect();

        this.background = new cc.Sprite(CLoaderScene.get('dialog_background_png'));
        this.background.setAnchorPoint(0.5, 0.5);
        this.addChild(this.background);

        this.title = new cc.Sprite(CLoaderScene.get('title_options_png'));
        this.title.setAnchorPoint(0.5, 0.5);
        this.addChild(this.title);

        this.soundOnOff = new CUtil.createButton({
            normal: CLoaderScene.get('button_sound_' + (CSound.isMuteSound() ? 'off' : 'on') + '_png'),
            pressed: CLoaderScene.get('button_sound_' + (CSound.isMuteSound() ? 'off' : 'on') + '_pushed_png')
        })
        this.soundOnOff.addTouchEventListener(function(e, type) {
            if (type == cc.EventTouch.EventCode.ENDED) {
                cc.audioEngine.playEffect(CLoaderScene.get('button_mp3'));

                var state = !CSound.isMuteSound();
                CSound.muteSound(state);
                CSound.muteMusic(state);

                Cookie.set(COOKIE_MUTE, state ? 1 : 0);
                Cookie.save();

                this.soundOnOff.loadTextureNormal(CLoaderScene.get('button_sound_' + (state ? 'off' : 'on') + '_png'), ccui.Widget.LOCAL_TEXTURE);
                this.soundOnOff.loadTexturePressed(CLoaderScene.get('button_sound_' + (state ? 'off' : 'on') + '_pushed_png'), ccui.Widget.LOCAL_TEXTURE);
            }
        }, this);
        this.addChild(this.soundOnOff);

        this.home = new CUtil.createButton({
            normal: CLoaderScene.get('button_home_png'),
            pressed: CLoaderScene.get('button_home_pushed_png')
        })
        this.home.addTouchEventListener(function(e, type) {
            if (type == cc.EventTouch.EventCode.ENDED) {
                cc.audioEngine.playEffect(CLoaderScene.get('button_mp3'));

                if (Cup.display.screen instanceof GameScreen) {
                    this.hide({}, function() {

                        Cup.display.showScreen(SCREEN_MENU, {}, function() {
                            Publisher.showAd();
                        });

                    }, this);

                } else {
                    this.hide();
                }
            }
        }, this);
        this.addChild(this.home);

        this.back = new CUtil.createButton({
            normal: CLoaderScene.get('button_back_png'),
            pressed: CLoaderScene.get('button_back_pushed_png')
        })
        this.back.addTouchEventListener(function(e, type) {
            if (type == cc.EventTouch.EventCode.ENDED) {
                cc.audioEngine.playEffect(CLoaderScene.get('button_mp3'));
                this.hide();
            }
        }, this);
        this.addChild(this.back);
    },
    postInit: function() {
        this._super();
    },
    preRelease: function(props) {
        this._super(props);
    },
    postRelease: function() {
        this._super();
    },
    resize: function() {
        this._super();

        var winSize = cc.director.getWinSize();
        var backgroundContentSize = this.background.getContentSize();

        var offset = 20;
        var titleContentSize = this.title.getContentSize();
        var soundOnOffContentSize = this.soundOnOff.getContentSize();
        var homeContentSize = this.home.getContentSize();
        var backContentSize = this.back.getContentSize();

        this.background.setPosition(winSize.width / 2, winSize.height / 2);
        this.title.setPosition(this.background.x, this.background.y + backgroundContentSize.height / 2 - titleContentSize.height / 2 - offset);
        this.soundOnOff.setPosition(this.title.x, this.title.y - titleContentSize.height / 2 - soundOnOffContentSize.height / 2 - offset);
        this.home.setPosition(this.soundOnOff.x, this.soundOnOff.y - soundOnOffContentSize.height / 2 - homeContentSize.height / 2 - offset);
        this.back.setPosition(this.home.x, this.home.y - homeContentSize.height / 2 - backContentSize.height / 2 - offset);
    },
    playShowEffect: function() {
        this.showEffectTimeoutId = setTimeout(function() {
            cc.audioEngine.playEffect(CLoaderScene.get('window_mp3'));
        }, 200);
    },
    stopShowEffect: function() {
        if (this.showEffectTimeoutId > -1) {
            clearTimeout(this.showEffectTimeoutId);
            this.showEffectTimeoutId = -1;
        }
    }
});