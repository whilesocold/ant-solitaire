var ScoreDialog = CDialog.extend({
    preInit: function(props) {
        this._super(props);

        this.playShowEffect();

        this.currentRank = props.currentRank;
        this.currentColor = props.currentColor;

        this.nextground = new cc.Sprite(CLoaderScene.get('dialog_background_png'));
        this.nextground.setAnchorPoint(0.5, 0.5);
        this.addChild(this.nextground);

        this.title = new cc.Sprite(CLoaderScene.get('title_score_cup_png'));
        this.title.setAnchorPoint(0.5, 0.5);
        this.addChild(this.title);

        this.bestTime = new LabelWithSprite(CLoaderScene.get('score_time_icon_png'), props.time.toString());
        this.bestTime.setOpacity(0);
        this.bestTime.setScale(0);
        this.addChild(this.bestTime);

        this.wild = new LabelWithSprite(CLoaderScene.get('score_joker_icon_png'), props.wild.toString());
        this.wild.setOpacity(0);
        this.wild.setScale(0);
        this.addChild(this.wild);

        this.cardRun = props.cardRun;

        this.card = new LabelWithSprite(CLoaderScene.get('score_card_icon_png'), props.card.toString());
        this.card.setOpacity(0);
        this.card.setScale(0);
        this.addChild(this.card);

        this.levelRound = new LabelWithSprite(CLoaderScene.get('score_level_round_icon_png'), props.levelRound.toString());
        this.levelRound.setOpacity(0);
        this.levelRound.setScale(0);
        this.addChild(this.levelRound);

        this.levelRoundLabel = new cc.LabelBMFont(props.levelRoundId.toString(), CLoaderScene.get('frankfurter_medium_26_fnt'));
        this.levelRoundLabel.setAnchorPoint(0.5, 0.5);
        this.levelRoundLabel.setPosition(27, 8);
        this.levelRound.addChild(this.levelRoundLabel);

        this.levelRoundId = props.levelRoundId;
        this.totalScore = props.total;

        this.cup = new LabelWithSprite(CLoaderScene.get('score_cup_icon_png'), this.totalScore.toString());
        this.cup.setOpacity(0);
        this.cup.setScale(0);
        this.addChild(this.cup);

        Cookie.set(COOKIE_SCORE, this.totalScore);
        Cookie.save();

        this.next = new CUtil.createButton({
            normal: CLoaderScene.get('button_next_png'),
            pressed: CLoaderScene.get('button_next_pushed_png')
        })
        this.next.addTouchEventListener(function(e, type) {
            if (type == cc.EventTouch.EventCode.ENDED) {
                cc.audioEngine.playEffect(CLoaderScene.get('button_mp3'));
                this.hide({}, function() {
                    Cup.display.showScreen(SCREEN_GAME, {
                        levelRound: this.levelRoundId + 1,
                        currentRank: this.currentRank,
                        currentColor: this.currentColor,
                        totalScore: this.totalScore,
                        cardRun: this.cardRun
                    });
                }, this);
            }
        }, this);
        this.addChild(this.next);
    },
    postInit: function() {
        this._super();
        this.showScore();
    },
    preRelease: function(props) {
        this._super(props);
    },
    postRelease: function() {
        this._super();
    },
    resize: function() {
        this._super();

        var winSize = cc.director.getWinSize();
        var backgroundContentSize = this.nextground.getContentSize();

        var titleOffsetY = 50;
        var backOffsetY = 20;
        var listStartX = 210;
        var listStartY = 40;
        var listOffset = 0;

        var backContentSize = this.next.getContentSize();
        var titleContentSize = this.title.getContentSize();
        var bestTimeContentSize = this.bestTime.getContentSize();
        var wildContentSize = this.wild.getContentSize();
        var cardContentSize = this.card.getContentSize();
        var levelRoundContentSize = this.levelRound.getContentSize();
        var cupContentSize = this.cup.getContentSize();

        this.nextground.setPosition(winSize.width / 2, winSize.height / 2);
        this.title.setPosition(this.nextground.x, this.nextground.y + backgroundContentSize.height / 2 + titleContentSize.height / 2 - 80);
        this.bestTime.setPosition(this.nextground.x - backgroundContentSize.width / 2 + listStartX, this.title.y - titleContentSize.height / 2 - bestTimeContentSize.height / 2 - listStartY);
        this.wild.setPosition(this.bestTime.x, this.bestTime.y - bestTimeContentSize.height / 2 - wildContentSize.height / 2 - listOffset);
        this.card.setPosition(this.wild.x, this.wild.y - wildContentSize.height / 2 - cardContentSize.height / 2 - listOffset);
        this.levelRound.setPosition(this.card.x, this.card.y - cardContentSize.height / 2 - levelRoundContentSize.height / 2 - listOffset);
        this.cup.setPosition(this.levelRound.x, this.levelRound.y - levelRoundContentSize.height / 2 - cupContentSize.height / 2 - listOffset);

        this.next.setPosition(this.nextground.x, this.nextground.y - backgroundContentSize.height / 2 + backContentSize.height / 2 + backOffsetY * 2);
    },
    showScore: function() {
        var nodes = [this.bestTime, this.wild, this.card, this.levelRound, this.cup];
        for (var i in nodes) {
            var node = nodes[i];
            var action = new cc.Sequence(
                new cc.DelayTime(0.2 * i),
                new cc.Spawn(
                    new cc.ScaleTo(0.2, 1),
                    new cc.FadeIn(0.2)
                ),
                new cc.CallFunc(function() {
                    cc.audioEngine.playEffect(CLoaderScene.get('score_add_mp3'));
                })
            );
            node.runAction(action);
        }
    },
    playShowEffect: function() {
        this.showEffectTimeoutId = setTimeout(function() {
            cc.audioEngine.playEffect(CLoaderScene.get('window_mp3'));
        }, 200);
    },
    stopShowEffect: function() {
        if (this.showEffectTimeoutId > -1) {
            clearTimeout(this.showEffectTimeoutId);
            this.showEffectTimeoutId = -1;
        }
    }
});