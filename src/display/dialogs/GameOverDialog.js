var GameOverDialog = CDialog.extend({
   preInit: function(props) {
      this._super(props);

      this.playShowEffect();

      this.background = new cc.Sprite(CLoaderScene.get('dialog_background_png'));
      this.background.setAnchorPoint(0.5, 0.5);
      this.addChild(this.background);

      this.title = new cc.Sprite(CLoaderScene.get('label_cup_highscrore_png'));
      this.title.setAnchorPoint(0.5, 0.5);
      this.addChild(this.title);

      this.star = new cc.Sprite(CLoaderScene.get('star_png'));
      this.star.setAnchorPoint(0.5, 0.5);
      this.addChild(this.star);

      this.totalLabel = new cc.LabelBMFont(CConfig.get(LANG_TOTAL_LABEL) + props.total.toString(), CLoaderScene.get('frankfurter_medium_40_fnt'));
      this.totalLabel.setAnchorPoint(0.5, 0.5);
      this.totalLabel.setPosition(27, 8);
      this.addChild(this.totalLabel);

      this.home = new CUtil.createButton({
         normal: CLoaderScene.get('button_home_png'),
         pressed: CLoaderScene.get('button_home_pushed_png')
      })
      this.home.addTouchEventListener(function(e, type) {
         if (type == cc.EventTouch.EventCode.ENDED) {
            cc.audioEngine.playEffect(CLoaderScene.get('button_mp3'));
            this.hide({}, function() {

               Cup.display.showScreen(SCREEN_MENU, {}, function() {
                  Publisher.showAd();
               });
            });
         }
      }, this);
      this.addChild(this.home);

      this.back = new CUtil.createButton({
         normal: CLoaderScene.get('button_back_png'),
         pressed: CLoaderScene.get('button_back_pushed_png')
      })
      this.back.addTouchEventListener(function(e, type) {
         if (type == cc.EventTouch.EventCode.ENDED) {
            cc.audioEngine.playEffect(CLoaderScene.get('button_mp3'));
            this.hide({}, function() {});
         }
      }, this);
      this.addChild(this.back);
   },
   postInit: function() {
      this._super();
   },
   preRelease: function(props) {
      this._super(props);
      this.stopShowEffect();
   },
   postRelease: function() {
      this._super();
   },
   resize: function() {
      this._super();

      var winSize = cc.director.getWinSize();
      var backgroundContentSize = this.background.getContentSize();

      var offset = 15;
      var titleContentSize = this.title.getContentSize();
      var starContentSize = this.star.getContentSize();
      var homeContentSize = this.home.getContentSize();
      var backContentSize = this.back.getContentSize();

      this.background.setPosition(winSize.width / 2, winSize.height / 2);
      this.title.setPosition(this.background.x, this.background.y);
      this.star.setPosition(this.title.x, this.title.y + titleContentSize.height / 2 + starContentSize.height / 2 + 18);
      this.home.setPosition(this.title.x - homeContentSize.width / 2, this.title.y - titleContentSize.height / 2 - homeContentSize.height / 2 - offset);
      this.back.setPosition(this.home.x + homeContentSize.width / 2 + backContentSize.width / 2 + offset, this.home.y);
      this.totalLabel.setPosition(this.title.x, 252);
   },
   playShowEffect: function() {
      this.showEffectTimeoutId = setTimeout(function() {
         cc.audioEngine.playEffect(CLoaderScene.get('window_mp3'));
      }, 200);
   },
   stopShowEffect: function() {
      if (this.showEffectTimeoutId > -1) {
         clearTimeout(this.showEffectTimeoutId);
         this.showEffectTimeoutId = -1;
      }
   }
});