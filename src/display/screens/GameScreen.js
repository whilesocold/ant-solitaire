var CARD_STATE_FACE = 'face';
var CARD_STATE_BACK = 'back';

var CARD_TYPE_NORMAL = 'normal';
var CARD_TYPE_REPLACEMENT = 'replacement';
var CARD_TYPE_WILD = 'wild';
var CARD_TYPE_TOWER = 'tower';

var CARD_TOWER_TYPE_NONE = 'tower_none';
var CARD_TOWER_TYPE_LEFT = 'tower_left';
var CARD_TOWER_TYPE_MID = 'tower_mid';
var CARD_TOWER_TYPE_RIGHT = 'tower_right';

var CardData = cc.Class.extend({
	ctor: function (rank, color) {
		this.rank = rank;
		this.color = color;
	},
	equal: function (rank, color) {
		return rank == this.rank && color == this.color;
	}
});

var Card = cc.Node.extend({
	ctor: function (data, state) {
		this._super();

		this.data = data;
		if (this.data) {
			this.rank = data.rank;
			this.colour = data.color;
		} else {
			this.rank = undefined;
			this.colour = undefined;
		}
		this.name = this.rank + this.colour;
		this.state = state || CARD_STATE_FACE;
		this.type = CARD_TYPE_NORMAL;
		this.towerType = CARD_TOWER_TYPE_NONE;
		this.flippable = false;
		this.touchable = false;
		this.target = cc.p(0, 0);
		this.startPosition = cc.p(0, 0);
		this.acceleration = 35;
		this.moveToCalback = null;

		this.sprite = new cc.Sprite(CLoaderScene.get(this.getSpriteName(this.state)));
		this.sprite.setAnchorPoint(0.5, 0.5);
		this.addChild(this.sprite);

		this.setContentSize(this.sprite.getContentSize());
	},
	release: function () {
		this.unschedule(this.onUpdate);
		this.stopAllActions();
	},
	getSpriteName: function (state) {
		return state == CARD_STATE_FACE ? this.name + '_png' : 'card_back_png';
	},
	flipToFace: function () {
		this.state = CARD_STATE_FACE;
		this.sprite.setTexture(CLoaderScene.get(this.getSpriteName(CARD_STATE_FACE)));
	},
	flipToBack: function () {
		this.state = CARD_STATE_BACK;
		this.sprite.setTexture(CLoaderScene.get(this.getSpriteName(CARD_STATE_BACK)));
	},
	flip: function () {
		if (this.state == CARD_STATE_FACE) {
			this.flipToBack();
		} else {
			this.flipToFace();
		}
	},
	fadeIn: function (delay) {
		var x = this.target.x;
		var y = this.target.y;
		var action = new cc.Sequence(
			new cc.DelayTime(delay),
			new cc.Spawn(
				new cc.MoveTo(0.5, x, y),
				new cc.RotateTo(0.5, 0)),
			new cc.CallFunc(function () {
				this.schedule(this.onUpdate);
			}, this)
		);
		this.runAction(action);
	},
	moveTo: function (x, y, callback) {
		this.target = cc.p(x, y);
		this.moveToCalback = callback;
	},
	onUpdate: function (dt) {
		var ydist = this.target.y - this.y;
		var xdist = this.target.x - this.x;
		var length = Math.sqrt(ydist * ydist + xdist * xdist);

		if (length < this.acceleration) {
			this.x = this.target.x;
			this.y = this.target.y;
			if (this.moveToCalback) {
				this.moveToCalback();
				this.moveToCalback = null;
			}
		} else {
			var radian = Math.atan2(ydist, xdist);
			this.x += Math.cos(radian) * this.acceleration;
			this.y += Math.sin(radian) * this.acceleration;
			//this.rotation = radian * 180 / Math.PI;
		}
	}
});

var Dealer = cc.Class.extend({
	ctor: function () {
		this.ranks = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"];
		this.colors = ['Club', 'Diamond', 'Heart', 'Spade'];
		this.dataList = [];
	},
	fill: function () {
		for (var i in this.colors) {
			var color = this.colors[i];
			for (var j in this.ranks) {
				var rank = this.ranks[j];
				this.add(rank, color);
			}
		}
		return this.dataList;
	},
	random: function () {
		this.dataList = CUtil.shuffleArray(this.dataList);
		return this.dataList;
	},
	add: function (rank, color) {
		this.dataList.push(new CardData(rank, color));
	},
	remove: function (rank, color) {
		var dataList = this.get(rank, color);
		if (data) {
			this.dataList.splice(this.cards.indexOf(dataList), 1);
		}
	},
	pop: function (count) {
		var res = [];
		for (var i = 0; i < count; i++) {
			res.push(this.dataList.pop());
		}
		return res;
	},
	popExt: function (rank, color) {
		for (var i in this.dataList) {
			var data = this.dataList[i];
			if (data.equal(rank, color)) {
				this.dataList.splice(i, 1);
				return data;
			}
		}
		return null;
	},
	length: function () {
		return this.dataList.length;
	},
	get: function (rank, color) {
		for (var i in this.dataList) {
			var data = this.dataList[i];
			if (data.equal(rank, color)) {
				return data;
			}
		}
		return null;
	},
	getAll: function () {
		return this.dataList;
	}
});

var Field = cc.Node.extend({
	ctor: function (screen, dealer) {
		this._super();

		this.screen = screen;
		this.dealer = dealer;
		this.touchable = true;
		this.undoCard = null;

		this.towerL = new cc.Sprite(CLoaderScene.get('tower_left_png'));
		this.towerL.setOpacity(0);
		this.towerL.setVisible(false);
		this.towerL.setScale(1.32);
		this.towerL.setAnchorPoint(1, 0);
		this.towerL.setPosition(0, -600);
		this.addChild(this.towerL);

		this.towerR = new cc.Sprite(CLoaderScene.get('tower_right_png'));
		this.towerR.setOpacity(0);
		this.towerR.setVisible(false);
		this.towerR.setScale(1.32);
		this.towerR.setAnchorPoint(0, 0);
		this.towerR.setPosition(0, -600);
		this.addChild(this.towerR);

		this.towerM = new cc.Sprite(CLoaderScene.get('tower_middle_png'));
		this.towerM.setOpacity(0);
		this.towerM.setVisible(false);
		this.towerM.setScale(1.32);
		this.towerM.setAnchorPoint(0.5, 0);
		this.towerM.setPosition(0, -600);
		this.addChild(this.towerM);

		var panelW = 640 * 1.65;
		var panelH = 100 * 1.65;
		this.panel = new cc.DrawNode();
		this.panel.drawRect(cc.p(-panelW / 2, -panelH / 2), cc.p(panelW / 2, panelH / 2), cc.color(255, 200, 32, 150), 0, cc.color(0, 0, 0, 0));
		this.panel.setAnchorPoint(0.5, 0.5);
		this.panel.setVisible(false);
		this.addChild(this.panel);

		this.cardList = [];
		this.replacementCardList = [];
		this.playableCardList = [];
		this.lastFlippedCardList = [];
		this.cardContainer = new cc.Node();
		this.addChild(this.cardContainer);

		this.undo = new CUtil.createButton({
			normal: CLoaderScene.get('button_restart_undo_png'),
			pressed: CLoaderScene.get('button_restart_undo_pushed_png')
		})
		this.undo.setPosition(186 * 1.7, -142 * 1.85);
		this.undo.setScale(0);
		this.undo.addTouchEventListener(function (e, type) {
			if (type == cc.EventTouch.EventCode.ENDED) {
				cc.audioEngine.playEffect(CLoaderScene.get('button_mp3'));
				this.doUndo();
			}
		}, this);
		this.disableUndo();
		this.addChild(this.undo);

		this.highscore = new CUtil.createButton({
			normal: CLoaderScene.get('button_highscore_png'),
			pressed: CLoaderScene.get('button_highscore_pushed_png')
		})
		this.highscore.setPosition(-185, -265);
		this.highscore.setScale(0);
		this.highscore.setTouchEnabled(false);
		this.highscore.addTouchEventListener(function (e, type) {
			if (type == cc.EventTouch.EventCode.ENDED) {
				cc.audioEngine.playEffect(CLoaderScene.get('lose_mp3'));

				/*
				var cardValue = this.replacementCardList.length * 1000;
				var totalValue = parseInt(this.screen.timeValue + cardValue + this.screen.scoreValue);
				var wildValue = this.screen.useWild ? 0 : 5000;
				var roundValue = this.screen.levelRoundValue * 1000;
				var absoluteTotalValue = totalValue + wildValue + roundValue;
				*/
				var absoluteTotalValue = this.screen.scoreValue;

				Cup.display.showScreen(DIALOG_GAMEOVER, {
					total: absoluteTotalValue
				}, function () {
					Publisher.showAd();
				});
			}
		}, this);
		this.addChild(this.highscore);
	},
	release: function () {
		this.stopNewGameEffect();
		this.undo.stopAllActions();

		for (var i in this.cardList) {
			var card = this.cardList[i];
			card.release();
		}
	},
	doUndo: function () {
		if (this.canUndo) {
			this.disableUndo();

			var card = this.currentCard;
			var type = card.type;

			// lock tower
			if (this.lastUnlockedTower) {
				this.lastUnlockedTower.stopAllActions();
				this.fadeOutTower(this.lastUnlockedTower, function () {
					if (this.lastUnlockedTower) {
						this.lastUnlockedTower.setVisible(false);
						this.lastUnlockedTower = null;
					}
				}.bind(this));
			}

			if (type == CARD_TYPE_TOWER) {

				this.addCardToPlayableList(card);
				this.addCardToList(card);

				this.screen.setCardValue(this.screen.cardValue - 1);
				this.screen.setScoreValue(this.screen.scoreValue - 200 * Math.max(1, this.screen.cardValue));

			} else if (type == CARD_TYPE_REPLACEMENT) {

				card.flip();
				this.addCardToList(card);

				if (this.replacementCardList.length > 0) {
					var firstZOrder = this.replacementCardList[this.replacementCardList.length - 1].getZOrder();
					card.setZOrder(firstZOrder - 1);
				}

				this.addCardToReplacementListAtFirst(card);

				this.checkHighscoreButton();

			} else if (type == CARD_TYPE_NORMAL) {

				this.addCardToPlayableList(card);
				this.addCardToList(card);

				this.screen.setCardValue(this.screen.cardValue - 1);
				this.screen.setScoreValue(this.screen.scoreValue - 200 * Math.max(1, this.screen.cardValue));

			} else if (type == CARD_TYPE_WILD) {

				this.useWild = false;
				this.addCardToList(card);

			}

			this.currentCard = this.prevCard;
			card.moveTo(card.startPosition);

			for (var i in this.lastFlippedCardList) {
				var card = this.lastFlippedCardList[i];
				if (card.state == CARD_STATE_FACE) {
					card.touchable = false;
					card.flip();
				}
			}
		}
	},
	enableUndo: function () {
		this.canUndo = true;
		this.undo.setOpacity(255);
		this.undo.setTouchEnabled(true);
	},
	disableUndo: function () {
		this.canUndo = false;
		this.undo.setOpacity(128);
		this.undo.setTouchEnabled(false);
	},
	playNewGameEffect: function () {
		this.newGameEffectTimeoutId = setTimeout(function () {
			cc.audioEngine.playEffect(CLoaderScene.get('new_game_mp3'));
		}, 600);
	},
	stopNewGameEffect: function () {
		if (this.newGameEffectTimeoutId > -1) {
			clearTimeout(this.newGameEffectTimeoutId);
			this.newGameEffectTimeoutId = -1;
		}
	},
	fill: function (currentRank, currentColor) {
		var currentData = null;
		if (currentRank && currentColor) {
			currentData = this.dealer.popExt(currentRank, currentColor);
		}

		var offset = 6;
		var flippedOffset = 23.5;
		var delay = 0.001;

		var startY = 140;

		this.flllTower(CARD_TOWER_TYPE_LEFT, -335, startY, offset, 0);
		this.flllTower(CARD_TOWER_TYPE_MID, 0, startY, offset, delay);
		this.flllTower(CARD_TOWER_TYPE_RIGHT, 335, startY, offset, delay * 2);

		var towerStartY = -345;
		this.towerL.setPosition(-110, towerStartY + 35);
		this.towerM.setPosition(20, towerStartY);
		this.towerR.setPosition(100, towerStartY + 35);

		var cardSize = this.cardList[0].getContentSize();

		this.fillLine(0, startY - cardSize.height * 1.5, offset, delay * 3);
		this.fillReplacement((cardSize.width + offset) * -4.08, startY - cardSize.height * 2.5 - offset * 5, flippedOffset, delay * 4);

		var firstCard = this.replacementCardList[0];
		var lastCard = this.replacementCardList[this.replacementCardList.length - 1];
		var lastTarget = lastCard.target;

		var current = this.fillCurrent(lastTarget.x + cardSize.width + offset * 1, lastTarget.y, delay * 5, currentData);
		var wild = this.fillWild(lastTarget.x + cardSize.width * 3 + offset * 13, lastTarget.y, delay * 6)

		current.setZOrder(firstCard.getZOrder() - 1);
		wild.setZOrder(firstCard.getZOrder() - 1);

		this.useWild = false;

		this.panel.setPosition(0, lastTarget.y);
		this.panel.setVisible(true);

		this.undo.runAction(new cc.Sequence(
			new cc.DelayTime(delay * 10),
			new cc.ScaleTo(0.2, 1.2)
		));

		this.playNewGameEffect();
	},
	flllTower: function (towerType, x, y, offset, delay) {
		var count = 6;
		var dataList = this.dealer.pop(count);
		var cardList = [];

		var winSize = cc.director.getWinSize();
		var leftTop = this.convertToNodeSpace(cc.p(-200, -200));
		leftTop.y *= -1;

		for (var i in dataList) {
			var data = dataList[i];
			var card = new Card(data, CARD_STATE_BACK);

			card.setPosition(leftTop);
			card.setRotation(90);
			card.flippable = true;
			card.type = CARD_TYPE_TOWER;
			card.towerType = towerType;

			cardList.push(card);
			this.addCard(card);
			this.addCardToPlayableList(card);
		}
		var a = cardList[0];

		var b = cardList[1];
		var c = cardList[2];

		var d = cardList[3];
		var e = cardList[4];
		var f = cardList[5];

		var cardSize = a.getContentSize();

		a.target = cc.p(x, y);

		b.target = cc.p(a.target.x - cardSize.width / 2 - offset / 2, a.target.y - cardSize.height / 2);
		c.target = cc.p(a.target.x + cardSize.width / 2 + offset / 2, a.target.y - cardSize.height / 2);

		d.target = cc.p(a.target.x - cardSize.width - offset, b.target.y - cardSize.height / 2);
		e.target = cc.p(a.target.x, b.target.y - cardSize.height / 2);
		f.target = cc.p(a.target.x + cardSize.width + offset, b.target.y - cardSize.height / 2);

		a.startPosition = a.target;
		b.startPosition = b.target;
		c.startPosition = c.target;
		d.startPosition = d.target;
		e.startPosition = e.target;
		f.startPosition = f.target;

		for (var i in cardList) {
			var card = cardList[i];
			card.fadeIn(delay + i / 20);
		}
		return cardList;
	},
	fillLine: function (x, y, offset, delay) {
		var count = 10;
		var dataList = this.dealer.pop(count);
		var cardList = [];

		var winSize = cc.director.getWinSize();
		var leftTop = this.convertToNodeSpace(cc.p(-200, -200));
		leftTop.y *= -1;

		var cardSize = this.cardList[0].getContentSize();
		var deckWidth = (cardSize.width + offset) * count;

		for (var i in dataList) {
			var data = dataList[i];
			var card = new Card(data, CARD_STATE_FACE);

			card.setPosition(leftTop);
			card.setRotation(90);
			card.touchable = true;
			card.target = (cc.p(-deckWidth / 2 + i * (cardSize.width + offset) + cardSize.width / 2, y));
			card.startPosition = card.target;

			cardList.push(card);

			this.addCard(card);
			this.addCardToPlayableList(card);
		}
		for (var i in cardList) {
			var card = cardList[i];
			card.fadeIn(delay + i / 20);
		}
	},
	fillReplacement: function (x, y, offset, delay) {
		var count = this.dealer.length() - 1;
		var dataList = this.dealer.pop(count);
		var cardList = [];

		var winSize = cc.director.getWinSize();
		var leftTop = this.convertToNodeSpace(cc.p(-200, -200));
		leftTop.y *= -1;

		//var cardSize = this.cardList[0].getContentSize();
		//var deckWidth = offset * count;

		for (var i in dataList) {
			var data = dataList[i];
			var card = new Card(data, CARD_STATE_BACK);

			card.setPosition(leftTop);
			card.setRotation(90);
			card.type = CARD_TYPE_REPLACEMENT;
			card.touchable = true;
			card.target = cc.p(x + (i * offset), y);
			card.startPosition = card.target;

			cardList.push(card);

			this.addCard(card);
			this.addCardToReplacementList(card);
		}

		for (var i in cardList) {
			var card = cardList[i];
			card.fadeIn(delay + i / 60);
		}
	},
	fillCurrent: function (x, y, delay, currentData) {
		var winSize = cc.director.getWinSize();
		var leftTop = this.convertToNodeSpace(cc.p(-200, -200));
		leftTop.y *= -1;

		var card = new Card(currentData ? currentData : this.dealer.pop(1)[0], CARD_STATE_FACE);
		card.setPosition(leftTop);
		card.setRotation(90);
		card.target = cc.p(x, y);
		card.startPosition = card.target;

		this.addCard(card);
		this.removeCardFromList(card);

		card.fadeIn(delay);

		this.prevCard = card;
		this.currentCard = card;
		return card;
	},
	fillWild: function (x, y, delay) {
		var winSize = cc.director.getWinSize();
		var leftTop = this.convertToNodeSpace(cc.p(-200, -200));
		leftTop.y *= -1;

		var card = new Card(null, CARD_STATE_BACK);
		card.setPosition(leftTop);
		card.setRotation(90);
		card.touchable = true;
		card.type = CARD_TYPE_WILD;
		card.target = (cc.p(x, y));
		card.startPosition = card.target;
		card.sprite.setTexture(CLoaderScene.get('joker_png'));

		this.addCard(card);

		card.fadeIn(delay);
		return card;
	},
	addCard: function (card) {
		this.addCardToList(card);
		this.cardContainer.addChild(card);
	},
	addCardToList: function (card) {
		this.cardList.push(card);
	},
	removeCardFromList: function (card) {
		this.cardList.splice(this.cardList.indexOf(card), 1);
	},
	addCardToPlayableList: function (card) {
		this.playableCardList.push(card);
	},
	removeCardFromPlayableList: function (card) {
		this.playableCardList.splice(this.playableCardList.indexOf(card), 1);
	},
	removeCard: function (card) {
		this.removeCardFromList(card);
		this.cardContainer.removeChild(card);
	},
	addCardToReplacementList: function (card) {
		this.replacementCardList.push(card);
	},
	addCardToReplacementListAtFirst: function (card) {
		this.replacementCardList.unshift(card);
	},
	testForFlip: function () {
		this.lastFlippedCardList = [];

		for (var i in this.cardList) {
			var cardA = this.cardList[i];

			var testCurrent = cardA != this.currentCard;
			var testType = cardA.type == CARD_TYPE_NORMAL || cardA.type == CARD_TYPE_TOWER;
			var testFlippable = cardA.flippable;

			if (testType && testCurrent && testFlippable) {
				var hitList = [];

				var contentSizeA = cardA.getContentSize();
				var boundsA = cc.rect(cardA.x - contentSizeA.width / 2, cardA.y - contentSizeA.height / 2, contentSizeA.width, contentSizeA.height);

				for (var j in this.cardList) {
					var cardB = this.cardList[j];
					var contentSizeB = cardB.getContentSize();
					var boundsB = cc.rect(cardB.x - contentSizeB.width / 2, cardB.y - contentSizeB.height / 2, contentSizeB.width, contentSizeB.height);

					var testCurrent = cardB != this.currentCard;

					var testType = cardB.type == CARD_TYPE_NORMAL || cardB.type == CARD_TYPE_TOWER;
					var testOrder = cardB.y < cardA.y;
					var testIntersects = cc.rectIntersectsRect(boundsA, boundsB);

					if (testCurrent && testOrder && testType && testIntersects) {
						hitList.push(cardB);
					}
				}
				if (cardA.state == CARD_STATE_BACK && hitList.length == 0) {
					cc.audioEngine.playEffect(CLoaderScene.get('card_mp3'));

					cardA.flip();
					cardA.touchable = true;

					this.lastFlippedCardList.push(cardA);
				}
			}
		}
	},
	fadeInTower: function (tower, callback) {
		var action = new cc.Sequence(
			new cc.FadeIn(1),
			new cc.CallFunc(function (scope, props) {
				var callback = props.callback;
				if (callback) {
					callback();
				}
			}, this, {
				callback: callback
			})
		);
		tower.runAction(action);
	},
	fadeOutTower: function (tower, callback) {
		var action = new cc.Sequence(
			new cc.FadeOut(1),
			new cc.CallFunc(function (scope, props) {
				var callback = props.callback;
				if (callback) {
					callback();
				}
			}, this, {
				callback: callback
			})
		);
		tower.runAction(action);
	},
	testComplete: function () {
		var testTower = function (type, tower, fadeInCallback) {
			if (!tower.visible) {
				var test = false;
				for (var i in this.cardList) {
					var card = this.cardList[i];
					if (card.towerType == type) {
						test = true;
						break;
					}
				}
				if (test == 0) {
					tower.visible = true;
					tower.stopAllActions();
					this.fadeInTower(tower, fadeInCallback);
					this.lastUnlockedTower = tower;
				}
			}
		}.bind(this);

		var onFadeIn = function () {
			var success = this.towerL.visible && this.towerM.visible && this.towerR.visible;
			if (success) {

				var cardValue = this.replacementCardList.length * 1000;
				var totalValue = parseInt(this.screen.timeValue + cardValue + this.screen.scoreValue);
				var wildValue = this.screen.useWild ? 0 : 5000;
				var roundValue = this.screen.levelRoundValue * 1000;
				var absoluteTotalValue = totalValue + wildValue + roundValue;

				cc.audioEngine.playEffect(CLoaderScene.get('victory_mp3'));

				Cup.display.showScreen(DIALOG_SCORE, {
					wild: wildValue,
					time: this.screen.timeValue,
					card: cardValue, //this.screen.cardValue,
					cardRun: this.screen.cardValue,
					levelRound: roundValue,
					levelRoundId: this.screen.levelRoundValue,
					score: this.screen.scoreValue,
					total: absoluteTotalValue,
					currentRank: this.currentCard.rank,
					currentColor: this.currentCard.colour
				}, function () {
					Publisher.showAd();
				});
			}
		}.bind(this);

		testTower(CARD_TOWER_TYPE_LEFT, this.towerL, onFadeIn);
		testTower(CARD_TOWER_TYPE_MID, this.towerM, onFadeIn);
		testTower(CARD_TOWER_TYPE_RIGHT, this.towerR, onFadeIn);
	},
	checkHighscoreButton: function () {
		if (this.replacementCardList.length == 0) {
			this.highscore.stopAllActions();
			this.highscore.runAction(new cc.Sequence(
				new cc.ScaleTo(0.2, 1),
				new cc.CallFunc(function () {
					this.highscore.setTouchEnabled(true);
				}.bind(this))
			));
		} else {
			this.highscore.stopAllActions();
			this.highscore.setTouchEnabled(true);
			this.highscore.runAction(new cc.Sequence(
				new cc.ScaleTo(0.2, 0)
			));
		}
	},
	touchEnd: function (event, position) {
		if (!this.touchable) {
			return;
		}

		var cardList = this.cardList.concat();
		cardList = cardList.sort(function (a, b) {
			return a.y < b.y ? -1 : 1;
		});

		for (var i in cardList) {
			var card = cardList[i];

			var localPosition = this.cardContainer.convertToNodeSpace(position);
			var contentSize = card.getContentSize();
			var bounds = cc.rect(card.x - contentSize.width / 2, card.y - contentSize.height / 2, contentSize.width, contentSize.height);

			if (cc.rectContainsPoint(bounds, localPosition)) {
				//this._testForFlip(card);

				if (card.touchable) {
					var current = this.currentCard;
					var x = current.x;
					var y = current.y;

					if (card.type == CARD_TYPE_WILD) {
						this.lastFlippedCardList = [];

						card.setZOrder(this.currentCard.getZOrder() + 1);

						this.touchable = false;
						this.prevCard == this.currentCard;
						this.currentCard = card;
						this.useWild = true;
						this.enableUndo();

						this.removeCardFromList(card);

						cc.audioEngine.playEffect(CLoaderScene.get('pick_card_mp3'));

						card.moveTo(x, y, function () {
							this.lastUnlockedTower = null;
							this.touchable = true;
							cc.audioEngine.playEffect(CLoaderScene.get('place_card_mp3'));
						}.bind(this));

					} else if (card.type == CARD_TYPE_REPLACEMENT) {
						this.lastFlippedCardList = [];

						card = this.replacementCardList.shift();
						card.setZOrder(this.currentCard.getZOrder() + 1);

						this.touchable = false;
						this.prevCard = this.currentCard;
						this.currentCard = card;
						this.enableUndo();

						this.removeCardFromList(card);

						cc.audioEngine.playEffect(CLoaderScene.get('pick_card_mp3'));

						card.flip();
						card.moveTo(x, y, function () {
							this.lastUnlockedTower = null;
							this.touchable = true;
							cc.audioEngine.playEffect(CLoaderScene.get('place_card_mp3'));

							this.checkHighscoreButton();

						}.bind(this));

						this.screen.setCardValue(0);

					} else {
						var ranks = this.dealer.ranks;
						var maxRank = ranks.length - 1;
						var currentRank = current.rank;
						var currentRankIdx = ranks.indexOf(currentRank);
						var prevRankIdx = currentRankIdx > 0 ? currentRankIdx - 1 : maxRank;
						var prevRank = ranks[prevRankIdx];
						var nextRankIdx = currentRankIdx < maxRank ? currentRankIdx + 1 : 0;
						var nextRank = ranks[nextRankIdx];
						var test = current.type == CARD_TYPE_WILD ? true : card.rank == nextRank || card.rank == prevRank;

						if (test) {
							card.setZOrder(this.currentCard.getZOrder() + 1);

							this.touchable = false;
							this.prevCard = this.currentCard;
							this.currentCard = card;
							this.enableUndo();

							cc.audioEngine.playEffect(CLoaderScene.get('pick_card_mp3'));

							this.removeCardFromPlayableList(card);
							this.removeCardFromList(card);
							this.testForFlip();

							card.moveTo(x, y, function () {
								this.lastUnlockedTower = null;
								this.touchable = true;
								cc.audioEngine.playEffect(CLoaderScene.get('place_card_mp3'));
								this.testComplete();
							}.bind(this));

							this.screen.setScoreValue(this.screen.scoreValue + 200 * Math.max(1, this.screen.cardValue));
							this.screen.setCardValue(this.screen.cardValue + 1);
						}
					}
				}
				break;
			}
		}
	},
	update: function (dt) {
		if (Cup.display.hasActiveDialogs()) {
			if (this.canUndo) {
				this.undo.setTouchEnabled(false);
			}
			this.highscore.setTouchEnabled(false);
		} else {
			if (this.canUndo) {
				this.undo.setTouchEnabled(true);
			}
			this.highscore.setTouchEnabled(true);
		}
	},
	resize: function () {
		var winSize = cc.director.getWinSize();
		var landscape = Cup.landscape;
		var mult = 1.75;

		if (landscape) {
			this.setContentSize(760 * mult, 620 * mult);
		} else {
			this.setContentSize(700 * mult, 620 * mult);
		}
		var contentSize = this.getContentSize();
		var scale = Math.min(1.8, winSize.width / contentSize.width);

		this.setScale(scale);
	}
});

var GameScreen = CScreen.extend({
	preInit: function (props) {
		this._super(props);

		this.dealer = new Dealer();
		this.dealer.fill();
		this.dealer.random();

		this.container = new cc.Node();
		this.addChild(this.container);

		this.resize();

		this.field = new Field(this, this.dealer);
		this.container.addChild(this.field);

		this.score = new LabelWithSprite(CLoaderScene.get('icon_score_png'), '0000');
		this.addChild(this.score);

		this.time = new LabelWithSprite(CLoaderScene.get('icon_clock_png'), '0000');
		this.addChild(this.time);

		this.card = new LabelWithSprite(CLoaderScene.get('icon_cards_run_png'), '0000');
		this.addChild(this.card);

		this.levelRound = new cc.Sprite(CLoaderScene.get('icon_level_round_png'));
		this.addChild(this.levelRound);

		this.levelRoundLabel = new cc.LabelBMFont('', CLoaderScene.get('frankfurter_medium_34_fnt'));
		this.levelRoundLabel.setAnchorPoint(0.5, 0.5);
		this.addChild(this.levelRoundLabel);

		this.timeSubValue = 166;

		if (props.hasOwnProperty('totalScore')) {
			this.setScoreValue(props.totalScore);
		} else {
			this.setScoreValue(0);
		}
		if (props.hasOwnProperty('cardRun')) {
			this.setCardValue(props.cardRun);
		} else {
			this.setCardValue(0);
		}
		this.setTimeValue(20000);

		var levelRound = 1;
		if (props.hasOwnProperty('levelRound')) {
			levelRound = props.levelRound;
		}
		this.setLevelRoundValue(levelRound);

		this.clockTimer = new CTimer(1000, function (props) {
			var self = props.scope;
			var time = self.timeValue;
			var sub = self.timeSubValue;

			time -= sub;
			if (time < 0) {
				time = 0;
				self.clockTimer.stop();
			}

			self.setTimeValue(time);
		}, {
			scope: this
		})

		this.options = new CUtil.createButton({
			normal: CLoaderScene.get('button_options_png'),
			pressed: CLoaderScene.get('button_options_pushed_png')
		})
		this.options.addTouchEventListener(function (e, type) {
			if (type == cc.EventTouch.EventCode.ENDED) {
				cc.audioEngine.playEffect(CLoaderScene.get('button_mp3'));
				Cup.display.showScreen(DIALOG_OPTIONS);
			}
		}, this);
		this.addChild(this.options);

		if (props.hasOwnProperty('currentRank') && props.hasOwnProperty('currentColor')) {
			this.field.fill(props.currentRank, props.currentColor);
		} else {
			this.field.fill(null, null);
		}
		this.resize();
	},
	postInit: function () {
		this._super();
		this.field.testComplete();
	},
	preRelease: function (props) {
		this.field.release();
		this._super(props);
	},
	postRelease: function () {
		this._super();
	},
	update: function (dt) {
		this._super(dt);

		if (Cup.display.hasActiveDialogs()) {
			this.options.setTouchEnabled(false);
		} else {
			this.options.setTouchEnabled(true);
		}

		if (this.field) {
			this.field.update(dt);
		}
	},
	resize: function () {
		this._super();

		var winSize = cc.director.getWinSize();
		this.container.setPosition(winSize.width / 2, winSize.height / 2);

		if (this.field) {
			this.field.resize();
		}

		if (this.score) {
			var scoreContentSize = this.score.getContentSize();
			var timeContentSize = this.time.getContentSize();
			var cardContentSize = this.card.getContentSize();
			var levelRoundContentSize = this.levelRound.getContentSize();
			var offset = 8;

			this.score.setPosition(30, winSize.height - 120);
			this.time.setPosition(this.score.x, this.score.y - scoreContentSize.height / 2 - timeContentSize.height / 2 - offset);
			this.card.setPosition(this.time.x, this.time.y - timeContentSize.height / 2 - cardContentSize.height / 2 - offset);

			this.levelRound.setPosition(winSize.width - levelRoundContentSize.width - 20, this.score.y - 30);
			this.levelRoundLabel.setPosition(this.levelRound.x, this.levelRound.y);

			var optionsContentSize = this.options.getContentSize();
			this.options.setPosition(this.levelRound.x, this.levelRound.y + levelRoundContentSize.height / 2 + optionsContentSize.height / 2 + 20);
		}
	},
	touchEnd: function (e, touchPosition) {
		this._super(e, touchPosition);
		this.field.touchEnd(e, touchPosition);
	},
	pause: function () {
		this._super();
		this.clockTimer.pause();
	},
	resume: function () {
		this._super();
		this.clockTimer.run();
	},
	blur: function () {
		this._super();
	},
	focus: function () {
		this._super();
	},

	//
	setScoreValue: function (value) {
		this.scoreValue = value;
		this.score.label.setString(this.scoreValue);
	},
	setTimeValue: function (value) {
		this.timeValue = value;
		this.time.label.setString(this.timeValue);
	},
	setCardValue: function (value) {
		this.cardValue = value;
		this.card.label.setString(this.cardValue);
	},
	setLevelRoundValue: function (value) {
		this.levelRoundValue = value;
		this.levelRoundLabel.setString(this.levelRoundValue);
	},
});
