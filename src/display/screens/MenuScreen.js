var MenuScreen = CScreen.extend({
	preInit: function (props) {
		this._super(props);

		var winSize = cc.director.getWinSize();

		this.logo = new cc.Sprite(CLoaderScene.get('logo_png'));
		this.logo.setAnchorPoint(0.5, 0.5);
		this.addChild(this.logo);

		this.play = CUtil.createButton({
			normal: CLoaderScene.get('button_play_png'),
			pressed: CLoaderScene.get('button_play_pushed_png')
		})
		this.play.setTouchEnabled(true);
		this.play.setAnchorPoint(0.5, 0.5);
		this.play.addTouchEventListener(function (e, type) {
			if (type == cc.EventTouch.EventCode.ENDED) {
				cc.audioEngine.playEffect(CLoaderScene.get('button_mp3'));
				this.playMusic();
				Cup.display.showScreen(SCREEN_GAME);
			}
		}, this);
		this.addChild(this.play);

		this.moreGames = CUtil.createButton({
			normal: CLoaderScene.get('button_moregames_png')
		})
		this.moreGames.setAnchorPoint(0.5, 0.5);
		this.moreGames.addTouchEventListener(function (e, type) {
			if (type == cc.EventTouch.EventCode.ENDED) {
				cc.audioEngine.playEffect(CLoaderScene.get('button_mp3'));
				this.playMusic();
				Publisher.moreGamesLink();
			}
		}, this);
		this.addChild(this.moreGames);

		this.highscoreContainer = new cc.Node();
		this.highscoreContainer.setCascadeOpacityEnabled(true);
		this.addChild(this.highscoreContainer);

		this.highscore = new cc.Sprite(CLoaderScene.get('icon_highscore_png'));
		this.highscore.setCascadeOpacityEnabled(true);
		this.highscore.setAnchorPoint(0, 0.5);
		this.highscoreContainer.addChild(this.highscore);

		var highscoreValue = Cookie.get(COOKIE_SCORE);
		if (highscoreValue == undefined || isNaN(highscoreValue)) {
			highscoreValue = '0';
		} else {
			highscoreValue = highscoreValue.toString();
		}

		this.highscoreLabel = new cc.LabelBMFont(highscoreValue, CLoaderScene.get('frankfurter_medium_48_fnt'));
		this.highscoreLabel.setAnchorPoint(0, 0.5);
		this.highscoreLabel.setCascadeOpacityEnabled(true);
		this.highscoreContainer.addChild(this.highscoreLabel);

		this.options = new CUtil.createButton({
			normal: CLoaderScene.get('button_options_png'),
			pressed: CLoaderScene.get('button_options_pushed_png')
		})
		this.options.setTouchEnabled(true);
		this.options.addTouchEventListener(function (e, type) {
			if (type == cc.EventTouch.EventCode.ENDED) {
				cc.audioEngine.playEffect(CLoaderScene.get('button_mp3'));
				this.playMusic();
				Cup.display.showScreen(DIALOG_OPTIONS);
			}
		}, this);
		this.addChild(this.options);
	},
	postInit: function () {
		this._super();
	},
	preRelease: function (props) {
		this._super(props);
	},
	postRelease: function () {
		this._super();
	},
	update: function (dt) {
		this._super(dt);
	},
	resize: function () {
		this._super();

		var winSize = cc.director.getWinSize();
		var logoContentSize = this.logo.getContentSize();
		var playContentSize = this.play.getContentSize();
		var moreGamesContentSize = this.moreGames.getContentSize();

		this.logo.setPosition(winSize.width / 2, winSize.height / 2 + logoContentSize.height / 2.5);
		this.play.setPosition(this.logo.x, this.logo.y - logoContentSize.height / 2 - playContentSize.height + 55);
		this.moreGames.setPosition(this.logo.x, this.logo.y - logoContentSize.height / 2 - playContentSize.height - 60);

		var highscoreContentSize = this.highscore.getContentSize();
		var highscoreLabelContentSize = this.highscoreLabel.getContentSize();
		var highscoreOffset = 20;

		this.highscoreLabel.setPosition(
			this.highscore.x + this.highscore.getContentSize().width + highscoreOffset,
			this.highscore.y);

		var highscoreContainerContentSize = cc.size(highscoreContentSize.width + highscoreLabelContentSize.width + highscoreOffset, Math.max(highscoreContentSize.height, highscoreLabelContentSize.height));
		this.highscoreContainer.setContentSize(highscoreContainerContentSize);
		this.highscoreContainer.setPosition(this.play.x - highscoreContainerContentSize.width / 2, this.play.y - playContentSize.height / 2 - highscoreContainerContentSize.height - 85);

		var optionsContentSize = this.options.getContentSize();
		var landscape = Cup.landscape;
		if (landscape) {
			this.options.setPosition(winSize.width - 300, winSize.height - optionsContentSize.height);
		} else {
			this.options.setPosition(winSize.width - 100, winSize.height - optionsContentSize.height);
		}
	},
	playMusic: function () {
		if (!IS_MUSIC_PLAYING) {
			IS_MUSIC_PLAYING = true;
			cc.audioEngine.playMusic(CLoaderScene.get('music_mp3'), true);
		}
	}
});
