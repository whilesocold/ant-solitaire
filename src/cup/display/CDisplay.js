var CDisplay = cc.Scene.extend({
    onEnter: function() {
        this._super();
        this.init();
        return true;
    },
    onExit: function() {
        this.release();
        this._super();
        return true;
    },
    init: function() {
        this.screen = null;

        this.stateHashMap = [];
        this.activeQueue = [];
        this.screenContainer = new cc.Node();
        this.dialogContainer = new cc.Node();
        this.lastActiveDialog = null;

        this.addChild(this.screenContainer);
        this.addChild(this.dialogContainer);
        this.schedule(this.update);

        if (cc.sys.isMobile) {
            if (cc.sys.capabilities.touches) {
                this.touchListener = cc.EventListener.create({
                    event: cc.EventListener.TOUCH_ONE_BY_ONE,
                    swallowTouches: true,
                    onTouchBegan: function(touch, event) {
                        this.sendTouchBeginEvent(event);
                        return true;
                    }.bind(this),
                    onTouchMoved: function(touch, event) {
                        this.sendMoveEvent(event);
                        return true;
                    }.bind(this),
                    onTouchEnded: function(touch, event) {
                        this.sendTouchEndEvent(event);
                        return true;
                    }.bind(this)
                });
            } else {
                // error
            }
        } else {
            if (cc.sys.capabilities.mouse) {
                this.touchListener = cc.EventListener.create({
                    event: cc.EventListener.MOUSE,
                    onMouseDown: function(event) {
                        this.sendTouchBeginEvent(event);
                    }.bind(this),
                    onMouseUp: function(event) {
                        this.sendTouchEndEvent(event);
                    }.bind(this),
                    onMouseMove: function(event) {
                        this.sendMoveEvent(event);
                    }.bind(this)
                });
            } else {
                // error
            }
        }
        this.initTouchListener();

        if (this.onRun) {
            this.onRun();
        }
    },
    initTouchListener: function() {
        cc.eventManager.addListener(this.touchListener, this);
    },
    sendTouchBeginEvent: function(event) {
        var position = undefined;
        if (cc.sys.isMobile) {
            var touches = event.getTouches();
            if (touches.length > 0) {
                position = touches[0]._point;
            }
        } else {
            position = event.getLocation();
        }

        if (this.activeQueue.length > 0) {
            var end = this.activeQueue[this.activeQueue.length - 1];
            if (end && end.touchEnable) {
                if (position !== undefined) {
                    if (end.nodeKind == CUP_KIND_SCREEN) {
                        if (!Cup.ui.touchBegin(event, position)) {
                            end.touchBegin(event, position);
                        }
                    } else {
                        end.touchBegin(event, position);
                    }
                }
            }
        }
    },
    sendTouchEndEvent: function(event) {
        var position = undefined;
        if (cc.sys.isMobile) {
            var touches = event.getTouches();
            if (touches.length > 0) {
                position = touches[0]._point;
            }
        } else {
            position = event.getLocation();
        }

        if (this.activeQueue.length > 0) {
            var end = this.activeQueue[this.activeQueue.length - 1];
            if (end && end.touchEnable) {
                if (position !== undefined) {
                    if (end.nodeKind == CUP_KIND_SCREEN) {
                        if (!Cup.ui.touchEnd(event, position)) {
                            end.touchEnd(event, position);
                        }
                    } else {
                        end.touchEnd(event, position);
                    }
                }
            }
        }
    },
    sendMoveEvent: function(event) {
        var position = undefined;
        if (cc.sys.isMobile) {
            var touches = event.getTouches();
            if (touches.length > 0) {
                position = touches[0]._point;
            }
        } else {
            position = event.getLocation();
        }

        if (this.activeQueue.length > 0) {
            var end = this.activeQueue[this.activeQueue.length - 1];
            if (end && end.touchEnable) {
                if (position !== undefined) {
                    if (end.nodeKind == CUP_KIND_SCREEN) {
                        if (!Cup.ui.touchBegin(event, position)) {
                            end.touchMove(event, position);
                        }
                    } else {
                        end.touchMove(event, position);
                    }
                }
            }
        }
    },
    release: function() {
        this.removeAll();
        this.unschedule(this.update);
        this.removeAllChildrenWithCleanup(true);
    },
    addScreen: function(key, layer) {
        layer.display = this;
        layer.name = key;
        this.stateHashMap[key] = layer;
    },
    removeScreen: function(key) {
        var layer = this.stateHashMap[key];
        if (layer) {
            this.hideScreen(layer);
            delete this.stateHashMap[key];
        }
    },
    removeAll: function() {
        this.stateHashMap.forEach(function(iter) {
            this.removeScreen(iter);
        });
    },
    getScreen: function(key) {
        return this.stateHashMap[key];
    },
    showScreen: function(key, props, callback, callbackScope) {
        if (!props) {
            props = {};
        }

        var screen = this.getScreen(key);
        if (screen) {

            if (this.screen && screen.nodeKind == CUP_KIND_SCREEN) {
                this.hideScreen(this.screen.name, {}, function() {
                    this.showScreen(key, props, callback, callbackScope);
                }, this);

            } else {
                if (!this.isScreenActive(key)) {
                    if (screen.nodeKind == CUP_KIND_SCREEN) {
                        Cup.ui.showState(screen.name);
                        this.screen = screen;
                    }

                    this.activeQueue.push(screen);
                    screen.preInit(props);

                    var callbackAction = new cc.CallFunc(function() {
                        var screen = arguments[1];
                        screen.postInit();

                        if (callback) {
                            callback.call(callbackScope || this);
                        }
                    }, this, screen);

                    var action = null;
                    if (screen.nodeKind == CUP_KIND_DIALOG) {
                        screen.content.opacity = 0;
                        screen.content.x = -cc.winSize.width;
                        action = new cc.Sequence(
                            new cc.DelayTime(0.2),
                            new cc.Spawn(
                                new cc.Sequence(
                                    new cc.MoveTo(0.2, cc.p(5, 0)).easing(cc.easeBackOut()),
                                    new cc.MoveTo(0.06, cc.p(0, 0)).easing(cc.easeBackInOut())),
                                new cc.FadeTo(0.2, 255).easing(cc.easeCubicActionOut())
                            ),
                            callbackAction);

                        screen.blackout.opacity = 0;
                        screen.blackout.stopAllActions();
                        screen.blackout.runAction(new cc.Sequence(new cc.DelayTime(0.2), new cc.FadeTo(0.15, 200)));

                        if (!Cup.paused) {
                            Cup.pause();
                        }

                    } else if (screen.nodeKind == CUP_KIND_SCREEN) {
                        screen.content.opacity = 0;
                        action = new cc.Sequence(
                            new cc.DelayTime(0.2),
                            new cc.FadeTo(0.15, 255),
                            callbackAction);
                    }

                    Cup.resize();
                    this.addChild(screen);
                    Cup.resize();

                    screen.content.stopAllActions();
                    screen.content.runAction(action);
                }
            }
        }
    },
    hideScreen: function(key, props, callback, callbackScope) {
        if (!props) {
            props = {};
        }

        var screen = this.stateHashMap[key];
        if (screen) {
            if (this.isScreenActive(key)) {
                screen.preRelease(props);

                var callbackAction = new cc.CallFunc(function(screen) {
                    var screen = arguments[1];

                    this.removeChild(screen);
                    screen.postRelease();

                    this.activeQueue.splice(this.activeQueue.indexOf(screen), 1);
                    if (screen.nodeKind == CUP_KIND_SCREEN) {
                        this.screen = null;
                    }
                    /* else {
                     this.lastActiveDialog = screen;
                     } */

                    if (callback) {
                        callback.call(callbackScope || this);
                    }

                    if (this.getActiveDialogs().length == 0 && Cup.paused) {
                        Cup.resume();
                    }

                }, this, screen);

                var action = null;
                if (screen.nodeKind == CUP_KIND_DIALOG) {
                    action = new cc.Sequence(
                        new cc.DelayTime(0.2),
                        new cc.Spawn(
                            new cc.MoveTo(0.15, cc.p(cc.winSize.width, 0)),
                            new cc.FadeTo(0.15, 0)),
                        callbackAction);

                    screen.blackout.stopAllActions();
                    screen.blackout.runAction(new cc.Sequence(new cc.DelayTime(0.2), new cc.FadeTo(0.15, 0)));

                } else if (screen.nodeKind == CUP_KIND_SCREEN) {
                    Cup.ui.hideState(key);
                    action = new cc.Sequence(
                        new cc.DelayTime(0.2),
                        new cc.FadeTo(0.15, 0),
                        callbackAction);
                }
                screen.content.stopAllActions();
                screen.content.runAction(action);
            }
        }
    },
    isScreenActive: function(key) {
        var screen = this.getScreen(key);
        return this.activeQueue.indexOf(screen) > -1;
    },
    getActiveDialogs: function() {
        var res = [];
        for (var i = 0; i < this.activeQueue.length; i++) {
            var node = this.activeQueue[i];
            if (node.nodeKind == CUP_KIND_DIALOG) {
                res.push(node);
            }
        }
        return res;
    },
    hasActiveDialogs: function() {
        return this.getActiveDialogs().length > 0;
    },
    pause: function() {
        if (this.screen) {
            this.screen.pause();
        }
    },
    resume: function() {
        if (this.screen) {
            this.screen.resume();
        }
    },
    blur: function() {
        if (this.screen) {
            this.screen.blur();
        }
    },
    focus: function() {
        if (this.screen) {
            this.screen.focus();
        }
    },
    update: function(dt) {
        //if (!Cup.paused) {
        for (var key in this.stateHashMap) {
            var screen = this.stateHashMap[key];
            //if (this.isScreenActive(key)) {
            if (screen.updateable) {
                screen.update(dt);
            }
            //}
        }
        //}
        if (this.updateCallback) {
            this.updateCallback(dt);
        }
    },
    resize: function() {
        for (var key in this.stateHashMap) {
            var screen = this.stateHashMap[key];
            if (this.isScreenActive(key)) {
                if (screen.resize) {
                    screen.resize();
                }
            }
        }
    }
});