var CScreen = cc.Layer.extend({
    ctor: function() {
        this._super();
        this.nodeKind = CUP_KIND_SCREEN;
        this.nodeState = CUP_STATE_POST_RELEASE;
        this.touchEnable = false;
        this.updateable = false;

        return true;
    },
    preInit: function(props) {
        this.props = props;
        this.nodeState = CUP_STATE_PRE_INIT;

        this.content = new cc.Node();
        this.content.setCascadeOpacityEnabled(true);
        this.addChildRAW(this.content);
    },
    postInit: function() {
        this.touchEnable = true;
        this.updateable = true;
        this.nodeState = CUP_STATE_POST_INIT;
    },
    preRelease: function(props) {
        this.updateable = false;
        this.touchEnable = false;
        this.nodeState = CUP_STATE_PRE_RELEASE;
    },
    postRelease: function() {
        this.nodeState = CUP_STATE_POST_RELEASE;
        this.stopAllActions();
        this.removeAllChildrenWithCleanup(true);
    },
    update: function(dt) {},
    resize: function() {},
    addChildRAW: function(child, localZOrder, tag) {
        cc.Node.prototype.addChild.call(this, child, localZOrder, tag);
        this._renderCmd._bakeForAddChild(child);
    },
    addChild: function(child, localZOrder, tag) {
        this.content.addChild(child, localZOrder, tag);
    },
    removeChildRAW: function(child, cleanup) {
        if (this._children.length === 0) {
            return;
        }
        if (cleanup === undefined) {
            cleanup = true;
        }
        if (this._children.indexOf(child) > -1) {
            this._detachChild(child, cleanup);
        }
        cc.renderer.childrenOrderDirty = true;
    },
    removeChild: function(child, cleanup) {
        this.content.removeChild(child, cleanup);
    },
    reorderChildRAW: function(child, zOrder) {
        cc.assert(child, cc._LogInfos.Node_reorderChild);
        cc.renderer.childrenOrderDirty = true;
        this._reorderChildDirty = true;
        child.arrivalOrder = cc.s_globalOrderOfArrival;
        cc.s_globalOrderOfArrival++;
        child._setLocalZOrder(zOrder);
    },
    reorderChild: function(child, zOrder) {
        this.content.reorderChild(child, zOrder);
    },
    hide: function(props, callback, callbackScope) {
        Cup.display.hideScreen(this.name, props, callback, callbackScope);
    },
    touchBegin: function(event, position) {},
    touchEnd: function(event, position) {},
    touchMove: function(event, position) {},
    pause: function() {},
    resume: function() {},
    blur: function() {},
    focus: function() {}
});