var CUINode = cc.Node.extend({

    showAction: undefined,
    hideAction: undefined,

    ctor: function (props) {
	   this._super();

	   this.screen = Cup.display.screen;
	   this.x = props.x || 0;
	   this.y = props.y || 0;
	   this.anchorX = props.hasOwnProperty('anchorX') ? props.anchorX : 0;//.5;
	   this.anchorY = props.hasOwnProperty('anchorY') ? props.anchorY : 0;//.5;

	   this.alignX = props.alignX || undefined;
	   this.alignY = props.alignY || undefined;
	   this.marginX = props.marginX || undefined;
	   this.marginY = props.marginY || undefined;

	   this.setCascadeOpacityEnabled(true);

	   if (props.spritesheet) {
		  this.sprite = new cc.Sprite(CUtil.getSpriteFromCache(props.spritesheet));
	   } else if (props.button) {
		  this.button = CUtil.createButton({
			 x: props.button.x || 0,
			 y: props.button.y || 0,
			 text: props.button.text || '',
			 normal: props.button.normal,
			 pressed: props.button.pressed || '',
			 callback: props.button.callback || null,
			 callbackScope: props.button.callbackScope || this
		  });
		  this.button.anchorX = props.button.hasOwnProperty('anchorX') ? props.button.anchorX : CAlign.anchorCenter();
		  this.button.anchorY = props.button.hasOwnProperty('anchorY') ? props.button.anchorY : CAlign.anchorCenter();

		  this.setContentSize(this.button.getContentSize());
		  this.addChild(this.button);
	   }
	   return true;
    },
    preInit: function (props) {
	   this.props = props;
	   this.x = props.x || this.x;
	   this.y = props.y || this.y;
	   this.ui = this.parent;
	   this.nodeKind = props.nodeKind || undefined;
	   this.nodeState = CUP_STATE_PRE_INIT;
    },
    postInit: function () {
	   this.nodeState = CUP_STATE_POST_INIT;
    },
    preRelease: function (props) {
	   this.nodeState = CUP_STATE_PRE_RELEASE;
    },
    postRelease: function () {
	   this.nodeState = CUP_STATE_POST_RELEASE;
	   this.stopAllActions();
	   //this.removeAllChildrenWithCleanup(true);
    },
    update: function (dt) {
    },
    resize: function () {
	   var x = 0;
	   var y = 0;

	   if (this.alignX !== undefined) {
		  if (this.alignX === CAlign.CENTER) {
			 x = CAlign.screenCenterX();
		  } else if (this.alignX === CAlign.LEFT) {
			 x = CAlign.screenLeft();
		  } else if (this.alignX === CAlign.RIGHT) {
			 x = CAlign.screenRight();
		  }
	   }
	   if (this.alignY !== undefined) {
		  if (this.alignY === CAlign.CENTER) {
			 y = CAlign.screenCenterY();
		  } else if (this.alignY === CAlign.TOP) {
			 y = CAlign.screenTop();
		  } else if (this.alignY === CAlign.BOTTOM) {
			 y = CAlign.screenBottom();
		  }
	   }

	   if (this.marginX !== undefined) {
		  x += this.marginX * Cup.scale;
	   }
	   if (this.marginY !== undefined) {
		  y -= this.marginY * Cup.scale;
	   }

	   if (this.button) {
		  var alignX = 0;
		  var alignY = 0;
		  if (this.alignX === CAlign.LEFT) {
			 alignX = 1;
		  } else if (this.alignX === CAlign.RIGHT) {
			 alignX = -1;
		  }
		  if (this.alignY === CAlign.TOP) {
			 alignY = 1;
		  } else if (this.alignY === CAlign.BOTTOM) {
			 alignY = -1;
		  }
		  x += (this.button.getBoundingBox().width / 2 * alignX) * Cup.scale;
		  y -= (this.button.getBoundingBox().height / 2 * alignY) * Cup.scale;
	   }

	   CAlign.reposition(this, x, y);
    },
    isActive: function () {
	   return this.nodeState == CUP_STATE_PRE_INIT || this.nodeState == CUP_STATE_POST_INIT;
    },
    touchBegin: function (event, position) {
    },
    touchEnd: function (event, position) {
    }
});
