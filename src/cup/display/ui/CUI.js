var CUI = cc.Layer.extend({
	onEnter: function () {
		this._super();
		this.init();
		return true;
	},
	onExit: function () {
		this.release();
		this._super();
		return true;
	},
	init: function () {
		this.stateHashMap = [];
		this.nodeHashMap = [];
	},
	release: function () {
		this.removeAll();
		this.removeAllChildrenWithCleanup(true);
	},
	addNode: function (key, node) {
		if (!this.hasNode(key)) {
			node.ui = this;
			node.name = key;
			this.nodeHashMap[key] = node;
		}
	},
	hasNode: function (key) {
		return this.nodeHashMap[key] !== undefined;
	},
	getNode: function (key) {
		if (this.hasNode(key)) {
			return this.nodeHashMap[key];
		}
		return null;
	},
	setState: function (name, keys) {
		this.stateHashMap[name] = keys;
	},
	hasState: function (name) {
		return this.getState(name) !== undefined;
	},
	getState: function (name) {
		return this.stateHashMap[name];
	},
	showState: function (name, props, callback, callbackScope) {
		if (!props) {
			props = {};
		}
		if (this.hasState(name)) {

			if (this.state) {
				this.hideState(this.state.name, {}, function () {
					this.showState(name, props, callback, callbackScope);
				}, this);

			} else {
				this.state = this.getState(name);
				if (this.state.length > 0) {
					for (var i = 0; i < this.state.length; i++) {
						var node = this.getNode(this.state[i]);

						if (!node.isActive()) {

							var action = node.showAction || undefined;
							if (action === undefined) {
								var callfunc = new cc.CallFunc(function () {
									if (callback) {
										callback.call(callbackScope);
									}
								}, this);
								node.opacity = 0;
								action = new cc.Sequence(new cc.FadeTo(0.10, 255), callfunc);
							}

							node.stopAllActions();
							node.preInit(props);

							this.addChild(node);

							node.postInit();
							node.runAction(action);
						}
					}
				} else {
					if (callback) {
						callback.call(callbackScope);
					}
				}
			}
		}
	},
	hideState: function (state, props, callback, callbackScope) {
		if (!props) {
			props = {};
		}
		if (this.hasState(state)) {

			var keys = this.getState(state);
			if (keys.length > 0) {

				for (var i = 0; i < keys.length; i++) {
					var node = this.getNode(keys[i]);

					if (node.isActive()) {

						var action = node.hideAction || undefined;
						if (action === undefined) {
							var callfunc = new cc.CallFunc(function () {
								var node = arguments[1];

								this.state = null;
								this.removeChild(node);

								node.postRelease();

								if (callback) {
									callback.call(callbackScope);
								}
							}, this, node);
							action = new cc.Sequence(new cc.FadeTo(0.10, 0), callfunc);
						}

						node.stopAllActions();
						node.preRelease(props);
						node.runAction(action);
					}
				}
			} else {
				this.state = null;
				if (callback) {
					callback.call(callbackScope);
				}
			}
		}
	},
	update: function (dt) {
		if (this.state) {
			for (var i = 0; i < this.state.length; i++) {
				var node = this.getNode(this.state[i]);
				if (node.isActive()) {
					if (node.update) {
						node.update(dt);
					}
				}
			}
		}
	},
	resize: function () {
		if (this.state) {
			for (var i = 0; i < this.state.length; i++) {
				var node = this.getNode(this.state[i]);
				if (node.isActive()) {
					if (node.resize) {
						node.resize();
					}
				}
			}
		}
	},

	touchBegin: function (event, position) {
		if (this.state) {
			for (var i = 0; i < this.state.length; i++) {
				var node = this.getNode(this.state[i]);
				if (node.isActive()) {
					if (cc.rectContainsPoint(node.getBoundingBoxToWorld(), position)) {
						node.touchBegin(event, position);
						return true;
					}
				}
			}
		}
		return false;
	},
	touchEnd: function (event, position) {
		if (this.state) {
			for (var i = 0; i < this.state.length; i++) {
				var node = this.getNode(this.state[i]);
				if (node.isActive()) {
					if (cc.rectContainsPoint(node.getBoundingBoxToWorld(), position)) {
						node.touchEnd(event, position);
						return true;
					}
				}
			}
		}
		return false;
	},
	isIntersectAny: function (point) {
		if (this.state) {
			for (var i = 0; i < this.state.length; i++) {
				var node = this.getNode(this.state[i]);
				if (node.isActive()) {

					var bounds = node.getBoundingBoxToWorld();
					if (cc.rectContainsPoint(bounds, point)) {
						return node;
					}
				}
			}
		}
		return null;
	}
});
