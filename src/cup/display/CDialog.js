var CDialog = CScreen.extend({
    ctor: function() {
        this._super();
        this.nodeKind = CUP_KIND_DIALOG;
        this.prevDialog = null;
        this.prevDialogProps = null;
        return true;
    },
    preInit: function(props) {
        this.blackout = new cc.LayerColor(cc.color(0, 0, 0, 0));
        this.addChildRAW(this.blackout);

        this._super(props);

        if (props.hasOwnProperty('backTo') && props.backTo != undefined) {
            this.backTo = props.backTo;
        }
        if (props.hasOwnProperty('backToProps') && props.backToProps != undefined) {
            this.backToProps = props.backToProps;
        }
    },
    postInit: function() {
        this._super();
    },
    preRelease: function(props) {
        this._super();
    },
    postRelease: function() {
        this._super();
    },
    update: function(dt) {
        this._super(dt);
    },
    resize: function() {
        this._super();

        this.blackout.width = cc.winSize.width;
        this.blackout.height = cc.winSize.height;
    },
    hideAndShowPrevious: function(hideCallback, hideCallbackScope, showCallback, showCallbackScope) {
        this.hide({}, function() {
            if (hideCallback) {
                hideCallback.call(hideCallbackScope);
            }
            if (this.backTo) {
                Cup.display.showScreen(this.backTo, this.backToProps, showCallback, showCallbackScope);
            }

            this.backTo = null;
            this.backToProps = null;

            return true;
        }, this);
        return false;
    }
});