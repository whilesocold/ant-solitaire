var Cup = {
    canvas: document.getElementById('canvas'),
    context: canvas.getContext('2d'),
    width: canvas.width,
    height: canvas.height,
    scale: 1,
    paused: false,

    setup: function(callback, scope) {
        this.callback = callback;
        this.callbackScope = scope;

        cc.view.enableRetina(true);
        cc.view.adjustViewPort(true);
        cc.view.setResolutionPolicy(cc.ResolutionPolicy.SHOW_ALL);
        cc.view.resizeWithBrowserSize(true);
        cc.view.enableAutoFullScreen(true);

        this.orientation = cc.ORIENTATION_PORTRAIT;
        this.landscape = false;

        cc.view.setResizeCallback(this.resize.bind(this));

        //window.addEventListener('resize', this.resize.bind(this));
        //window.addEventListener('orientationchange', this.orientationChange.bind(this));
        window.addEventListener('blur', this.blur.bind(this));
        window.addEventListener('focus', this.focus.bind(this));

        this.resize();
        //this.orientationChange();

        CLoader.setup();
        CConfig.setup(this.onLangSetup);
    },
    onLangSetup: function() {
        CLoaderScene.setup();

        this.display = new CDisplay();
        this.display.updateCallback = this.update.bind(this);

        if (!this.loader) {
            this.loader = new CCLoaderSceneCustom();
        }
        this.callback.call(this.callbackScope);
    },
    setSceneLoader: function(value) {
        this.loader = value;
    },
    start: function(callback, scope) {
        this.display.onRun = function() {
            this.ui = new CUI();
            this.display.addChild(this.ui);
            this.audio = cc.audioEngine;
            this.resize();

            callback.call(scope);
        }.bind(this);

        cc.director.runScene(this.display);
        cc.TransitionMoveInL
    },
    pause: function() {
        if (!this.paused) {
            this.paused = true;
            this.display.pause();
        }
    },
    resume: function() {
        if (this.paused) {
            this.paused = false;
            this.display.resume()
        }
    },
    update: function(dt) {
        this.ui.update(dt);
    },
    resize: function() {
        var frameSize = cc.view.getFrameSize();
        var w = frameSize.width; //window.innerWidth;
        var h = frameSize.height; // window.innerHeight;

        if (w > h) {
            this.orientation = cc.ORIENTATION_LANDSCAPE_LEFT;
            this.landscape = true;
            cc.view.setDesignResolutionSize(this.height, this.width, cc.ResolutionPolicy.SHOW_ALL);
        } else {
            this.orientation = cc.ORIENTATION_PORTRAIT;
            this.landscape = false;
            cc.view.setDesignResolutionSize(this.width, this.height, cc.ResolutionPolicy.SHOW_ALL);
        }

        //cc.view.setDesignResolutionSize(w, h, cc.ResolutionPolicy.SHOW_ALL);

        //var w = window.innerWidth;
        //var h = window.innerHeight;
        //this.scale = Math.min(w / this.width, h / this.height);

        //cc.view.setDesignResolutionSize(w, h, cc.ResolutionPolicy.NO_BORDER);

        if (this.onResizeCallback) {
            this.onResizeCallback();
        }
        if (this.display) {
            this.display.resize();
        }
        if (this.ui) {
            this.ui.resize();
        }
        if (this.loader) {
            this.loader.resize();
        }
    },
    orientationChange: function() {
        /*
            var w = window.innerWidth;
            var h = window.innerHeight;

            if (w > h) {
                this.orientation = cc.ORIENTATION_LANDSCAPE_LEFT;

            } else {
                this.orientation = cc.ORIENTATION_PORTRAIT;
            }
            this.resize();
            */
        /*
          window.orientation = 90;
          switch (window.orientation) {
              case 0: // portrait
                  this.orientation = cc.ORIENTATION_PORTRAIT;
                  break;

              case 180: // portrait (upside-down)
                  this.orientation = cc.ORIENTATION_PORTRAIT_UPSIDE_DOWN;
                  break;

              case -90: // landscape (clockwise)
                  this.orientation = cc.ORIENTATION_LANDSCAPE_RIGHT;
                  break;

              case 90: // landscape  (counterclockwise)
                  this.orientation = cc.ORIENTATION_LANDSCAPE_LEFT;
                  break;
          }
          */
    },
    blur: function() {
        if (this.display) {
            this.display.blur();
        }
    },
    focus: function() {
        if (this.display) {
            this.display.focus();
        }
    }
};
