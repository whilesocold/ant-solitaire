var CSceneNode = cc.Node.extend({
    preInit: function (props) {
	   this.props = props;
	   this.scene = props.scene || undefined;
	   //this.x = props.x || 0;
	   //this.y = props.y || 0;
	   this.display = this.parent;
	   this.updateable = props.updateable || true;
	   this.nodeKind = props.nodeKind || undefined;
	   this.nodeState = CUP_STATE_PRE_INIT;
    },
    postInit: function () {
	   this.nodeState = CUP_STATE_POST_INIT;
    },
    preRelease: function (props) {
	   this.nodeState = CUP_STATE_PRE_RELEASE;
    },
    postRelease: function () {
	   this.nodeState = CUP_STATE_POST_RELEASE;
	   this.stopAllActions();
	   this.removeAllChildrenWithCleanup(true);
    },
    update: function (dt) {
    },
    isActive: function () {
	   return this.nodeState == CUP_STATE_PRE_INIT || this.nodeState == CUP_STATE_POST_INIT;
    },
    pause: function () {
    },
    resume: function () {
	}
});
