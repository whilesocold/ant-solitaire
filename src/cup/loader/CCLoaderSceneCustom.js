var CCLoaderSceneCustom = cc.Scene.extend({
    _interval: null,
    _label: null,
    _className: "CCLoaderSceneCustom",
    cb: null,
    target: null,

    init: function() {
        var self = this;

        self._bgLayer = new cc.LayerColor(cc._loaderBgColor);
        self.addChild(self._bgLayer, 0);

        self._bgContainer = new cc.Node();
        self.addChild(self._bgContainer);

        if (cc._loaderImage) {
            cc.loader.loadImg(cc._loaderImage, {
                isCrossOrigin: false
            }, function(err, img) {
                self._initStage(img);
            });
        }
        return true;
    },

    _initStage: function(img) {
        this._texture2d = new cc.Texture2D();
        this._texture2d.initWithElement(img);
        this._texture2d.handleLoadedTexture();

        this._logo = new cc.Sprite(this._texture2d);
        this._logo.setScale(cc.contentScaleFactor());
        this._logo.setAnchorPoint(0.5, 0.5);

        this._bgContainer.addChild(this._logo, 10);

        var logoWidth = img.width;
        var logoHeight = img.height;
        var fontSize = 48;
        var lblHeight = -logoHeight / 2 - 40;

        var label = this._label = new cc.LabelTTF("Loading... 0%", "Arial", fontSize);
        label.setAnchorPoint(0.5, 0.5);
        label.setPosition(0, lblHeight);
        label.setColor(cc.color(255, 255, 255));

        this._bgContainer.addChild(this._label, 10);

        this.onResize();
    },

    onEnter: function() {
        cc.Node.prototype.onEnter.call(this);
        this.schedule(this._startLoading, 0.3);
    },
    onExit: function() {
        cc.Node.prototype.onExit.call(this);
        var tmpStr = "Loading... 0%";
        this._label.setString(tmpStr);
    },

    initWithResources: function(resources, cb, target) {
        if (cc.isString(resources)) {
            resources = [resources];
        }
        this.resources = resources || [];
        this.cb = cb;
        this.target = target;
    },

    _startLoading: function() {
        var self = this;
        self.unschedule(self._startLoading);

        var res = self.resources;
        cc.loader.load(res,
            function(result, count, loadedCount) {
                var percent = (loadedCount / count * 100) | 0;
                percent = Math.min(percent, 100);
                self._label.setString("Loading... " + percent + "%");
            },
            function() {
                if (self.cb)
                    self.cb.call(self.target);
            });
    },

    _updateTransform: function() {
        this._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._bgLayer._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._bgContainer._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._label._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
        this._logo._renderCmd.setDirtyFlag(cc.Node._dirtyFlags.transformDirty);
    },

    onResize: function() {
        this._bgLayer.width = cc.winSize.width;
        this._bgLayer.height = cc.winSize.height;
        CAlign.reposition(this._bgContainer, CAlign.screenCenterX(), CAlign.screenCenterY());
    },

    preload: function(resources, cb, target) {
        var self = this;
        self.init();

        cc.eventManager.addCustomListener(cc.Director.EVENT_PROJECTION_CHANGED, function() {
            self._updateTransform();
        });

        self.initWithResources(resources, cb, target);
        cc.director.runScene(self);
    }
});