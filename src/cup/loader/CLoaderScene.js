var CLoaderScene = {
	LOADED_EVENT: 'loaderSceneLoadedEvent',

	setup: function () {
		this.nodeList = [];
		this.listener = cc.EventListener.create({
			event: cc.EventListener.CUSTOM,
			eventName: CLoaderScene.LOADED_EVENT,
			callback: function (e) {
			}
		});
		this.hashMap = [];
		this.loadedEvent = new cc.EventCustom(CLoaderScene.LOADED_EVENT);
	},
	add: function (key, path) {
		this.hashMap[key] = path;
	},
	get: function (key) {
		return this.hashMap[key];
	},
	start: function () {
		var list = [];
		for (var key in this.hashMap) {
			list.push(this.hashMap[key]);
		}
		Cup.loader.preload(list, this.onLoaded, this);
	},
	onLoaded: function (error) {
		if (error) {
			cc.error(error);
		} else {
			cc.eventManager.dispatchEvent(this.loadedEvent);
		}
	},
	addListener: function (callback, scope) {
		var listener = new cc._EventListenerCustom(this.listener._getListenerID(), function (event) {
			callback.call(scope, event);
		});
		cc.eventManager.addListener(listener, 1);
		this.nodeList.push(listener);
		return listener;
	},
	removeListener: function (listener) {
		var index = this.nodeList.indexOf(listener);
		if (index > -1) {
			this.nodeList.splice(index);
			return true;
		}
		return false;
	},
	removeListeners: function () {
		this.nodeList.forEach(function (iter) {
			cc.eventManager.removeListener(iter);
		});
		this.nodeList = [];
	}
};