var CConfig = {
    setup: function(callback) {
        this.callback = callback;
        this.hashMap = [];

        CLoader.add('strings_json', 'res/strings.json');
        CLoader.addListener(this.onLoad, this);
        CLoader.start();
    },
    onLoad: function(e) {
        var json = CLoader.get('strings_json');
        for (var key in json) {
            var value = json[key];
            this.hashMap[key] = value;
        }
        CLoader.removeListeners();
        this.callback.call(Cup);
    },
    get: function(key) {
        return this.hashMap[LANGUAGE][key];
    }
};