var CUtil = {
    disableCocosLoader: function() {
        if (!cc.sys.isNative && document.getElementById("cocosLoading")) {
            document.body.removeChild(document.getElementById("cocosLoading"));
        }
    },
    createButton: function(props) {
        var instance = new ccui.Button();

        instance.x = props.x || 0;
        instance.y = props.y || 0;
        instance.setTouchEnabled(true);
        instance.setPressedActionEnabled(true);

        if (props.hasOwnProperty('text')) {
            instance.titleFontName = props.font || LABEL_FONT;
            instance.titleFontSize = props.fontSize || 48;
            instance.setTitleText(props.text || '');
            instance.getTitleRenderer().setColor(props.textColor || cc.color(255, 255, 255));
            instance.getTitleRenderer().anchorX = props.textAnchorX || CAlign.anchorCenter();
            instance.getTitleRenderer().anchorY = props.textAnchorY || CAlign.anchorCenter();
        }
        instance.loadTextureNormal(props.normal, ccui.Widget.LOCAL_TEXTURE);
        if (props.hasOwnProperty('pressed')) {
            instance.loadTexturePressed(props.pressed, ccui.Widget.LOCAL_TEXTURE);
        }
        return instance;
    },
    createButton2: function(props) {
        var w = props.width;
        var h = props.height;
        var text = props.text;
        var fillColor = props.fillColor ? props.fillColor : cc.color.ORANGE;

        var button = new cc.DrawNode();
        button.anchorX = 0.5;
        button.anchorY = 0.5;
        button.setContentSize(w, h);
        button.drawRect(cc.p(0, 0), cc.p(w, h), fillColor, 1, cc.color.BLACK);

        var label = new cc.LabelTTF(text, FONT_NAME, 32);
        label.anchorX = 0.5;
        label.anchorY = 0.5;
        label.x = w / 2;
        label.y = h / 2;
        label.setFontFillColor(cc.color.BLACK);

        button.label = label;
        button.addChild(label);

        return button;
    },

    createSpriteSheet: function(plist) {
        cc.spriteFrameCache.addSpriteFrames(CLoaderScene.get(plist));
    },
    getSpriteFromCache: function(name) {
        return cc.spriteFrameCache.getSpriteFrame(name);
    },

    nodeContainsPoint: function(node, point) {
        return cc.rectContainsPoint(node.getBoundingBoxToWorld(), point);
    },

    shuffleArray: function(a) {
        var j, x, i;
        for (i = a.length; i; i -= 1) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
        return a;
    },

    remapValue: function(value, a, b, c, d) {
        return c + (d - c) * (value - a) / (b - a);
    }
};

var CAction = {
    buttonTap: function(noScale) {
        var s = noScale ? 1 : Cup.scale;
        return new cc.Sequence(
            new cc.ScaleTo(0.15, 1.2 * s, 0.8 * s).easing(cc.easeSineInOut()),
            new cc.ScaleTo(0.15, 1 * s).easing(cc.easeSineInOut())
        );
    }
};

var CMath = {
    distance: function(a, b) {
        var xs = 0;
        var ys = 0;
        xs = b.x - a.x;
        xs = xs * xs;
        ys = b.y - a.y;
        ys = ys * ys;
        return Math.sqrt(xs + ys);
    }
};

var CAlign = {
    LEFT: 1,
    RIGHT: 2,
    TOP: 3,
    BOTTOM: 4,
    CENTER: 5,

    screenTop: function() {
        return cc.winSize.height;
    },
    screenBottom: function() {
        return 0;
    },
    screenLeft: function() {
        return 0;
    },
    screenRight: function() {
        return cc.winSize.width;
    },
    screenCenterX: function() {
        return cc.winSize.width / 2;
    },
    screenCenterY: function() {
        return cc.winSize.height / 2;
    },
    screenCenterCenter: function() {
        return cc.p(this.screenCenterW(), this.screenCenterX());
    },
    screenCenterTop: function() {
        return cc.p(this.screenCenterX(), this.screenTop());
    },
    screenCenterBottom: function() {
        return cc.p(this.screenCenterX(), this.screenBottom());
    },
    screenLeftTop: function() {
        return cc.p(this.screenLeft(), this.screenTop());
    },
    screenLeftBottom: function() {
        return cc.p(this.screenLeft(), this.screenBottom());
    },
    screenRightTop: function() {
        return cc.p(this.screenRight(), this.screenTop());
    },
    screenRightBottom: function() {
        return cc.p(this.screenRight(), this.screenBottom());
    },

    anchorCenter: function() {
        return 0.5;
    },
    anchorTop: function() {
        return 1;
    },
    anchorBottom: function() {
        return 0;
    },
    anchorLeft: function() {
        return 0;
    },
    anchorRight: function() {
        return 1;
    },
    anchor: function(node, x, y) {
        node.anchorX = x;
        node.anchorY = y;
    },
    reposition: function(node, x, y) {
        node.setScale(Cup.scale);
        node.setPosition(x, y);
    }
};

var CSound = {
    musicVolume: 0.6,
    setMusicVolume: function(value) {
        this.musicVolume = value;
        cc.audioEngine.setMusicVolume(value);
    },
    soundVolume: 1,
    setSoundVolume: function(value) {
        this.soundVolume = value;
        cc.audioEngine.setEffectsVolume(value);
    },
    muteMusic: function(value) {
        if (value === true || value == 1) {
            cc.audioEngine.setMusicVolume(0);
        } else {
            cc.audioEngine.setMusicVolume(this.musicVolume);
        }
    },
    muteSound: function(value) {
        if (value === true || value == 1) {
            cc.audioEngine.setEffectsVolume(0);
        } else {
            cc.audioEngine.setEffectsVolume(this.soundVolume);
        }
    },
    isMuteMusic: function() {
        return cc.audioEngine.getMusicVolume() == 0;
    },
    isMuteSound: function() {
        return cc.audioEngine.getEffectsVolume() == 0;
    }
};

var CAnimation = {
    createAnimation: function(keys, interval) {
        var animFrames = [];
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var frame = cc.spriteFrameCache.getSpriteFrame(key);
            animFrames.push(frame);
        }
        return new cc.Animation(animFrames, interval);
    }
};

var CSetInterval = function(callback, delay) {
    var timerId, start, remaining = delay;
    this.pause = function() {
        window.clearTimeout(timerId);
        remaining -= new Date() - start;
    };
    var resume = function() {
        start = new Date();
        timerId = window.setTimeout(function() {
            remaining = delay;
            resume();
            callback();
        }, remaining);
    };
    this.resume = resume;
    this.resume();
};

var CSetTimeout = function(callback, delay) {
    var timerId, start, remaining = delay;
    this.pause = function() {
        window.clearTimeout(timerId);
        remaining -= new Date() - start;
    };
    this.resume = function() {
        start = new Date();
        window.clearTimeout(timerId);
        timerId = window.setTimeout(callback, remaining);
    };
    this.resume();
};

var CBase64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function(e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = CBase64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64;
            } else if (isNaN(i)) {
                a = 64;
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a);
        }
        return t
    },
    decode: function(e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r);
            }
            if (a != 64) {
                t = t + String.fromCharCode(i);
            }
        }
        t = CBase64._utf8_decode(t);
        return t
    },
    _utf8_encode: function(e) {
        e = e.replace(/\r\n/g, "\n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128);
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128);
            }
        }
        return t;
    },
    _utf8_decode: function(e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++;
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2;
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3;
            }
        }
        return t;
    }
};

function CTimer(delay, callbacks, props) {
    if (Object.prototype.toString.call(callbacks) === "[object Function]") {
        callbacks = [callbacks];
    }
    this.callbacks = callbacks;
    this.props = props;
    var that = this;
    var id = setInterval(function tick() {
        if (!that.running) return;
        for (var i = 0; i < that.callbacks.length; i++) {
            that.callbacks[i].call(that, props);
        }
        that.count++;
    }, delay);
    this.__defineGetter__('id', function() {
        return id
    });
    this.__defineGetter__('delay', function() {
        return delay
    });
    CTimer.all.push(this);
}
CTimer.prototype.running = true;
CTimer.prototype.count = 0;
CTimer.prototype.pause = function pause() {
    this.running = false;
    return this;
};
CTimer.prototype.run = function run() {
    this.running = true;
    return this;
};
CTimer.prototype.stop = function stop() {
    // Unlike pause, once you stop timer, you can't run it again
    clearInterval(this.id);
    this.stopped = true;
    return this;
};
CTimer.all = [];
CTimer.all.pause = function pause() {
    var all = CTimer.all;
    for (var i = 0; i < all.length; i++) {
        all[i].pause();
    }
    return all;
};
CTimer.all.run = function run() {
    var all = CTimer.all;
    for (var i = 0; i < all.length; i++) {
        all[i].run();
    }
    return all;
};
CTimer.all.stop = function stop() {
    var all = CTimer.all;
    for (var i = 0; i < all.length; i++) {
        all[i].stop();
    }
    return all;
};