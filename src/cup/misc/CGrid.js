var CGrid = cc.Node.extend({
    ctor: function (width, height, cellWidth, cellHeight, lineColor) {
	   this._super();

	   this.width = width;
	   this.height = height;
	   this.cellWidth = cellWidth;
	   this.cellHeight = cellHeight;
	   this.lineColor = lineColor;

	   this.node = new cc.DrawNode();
	   this.addChild(this.node);

	   this.invalidate();
    },
    invalidate: function () {
	   this.clear();
	   this.render();
    },
    render: function () {
	   for (var x = 0; x <= this.width; x++) {
		  for (var y = 0; y <= this.height; y++) {
			 this.node.drawRect(cc.p(x * this.cellWidth, y * this.cellHeight), cc.p(this.cellWidth, this.cellHeight), null, 1, this.lineColor);
		  }
	   }
    },
    clear: function () {
	   this.node.clear();
    }
});
