var CCookie = {
    isSupport: function() {
        if (typeof localStorage === 'object') {
            try {
                localStorage.setItem('localStorage', 1);
                localStorage.removeItem('localStorage');
                return true;
            } catch (e) {
                Storage.prototype._setItem = Storage.prototype.setItem;
                Storage.prototype.setItem = function() {};
                return false;
            }
        }
        return false;
    },
    set: function(key, value) {
        if (window.famobi.localStorage) {
            window.famobi.localStorage.setItem(key, value);
        } else {
            if (this.isSupport()) {
                window.localStorage.setItem(key, value);
            }
        }
    },
    get: function(key) {
        var value = null;
        if (window.famobi.localStorage) {
            value = window.famobi.localStorage.getItem(key);
        } else {
            value = this.isSupport() ? window.localStorage.getItem(key) : null;
        }
        return value;
    },
    remove: function(key) {
        if (window.famobi.localStorage) {
            window.famobi.localStorage.removeItem(key);

        } else {
            if (this.isSupport()) {
                window.localStorage.removeItem(key);
            }
        }
    },
    clear: function() {
        if (window.famobi.localStorage) {
            window.famobi.localStorage.clear();
        } else {
            if (this.isSupport()) {
                window.localStorage.clear();
            }
        }
    }
};