var App = {
	setLanguage: function (language) {
		this.language = language;
	},
	getLanguage: function () {
		return this.language;
	},
	onStart: function () {
		Cup.setSceneLoader(new SceneLoader());
		Cup.setup(function () {
			CLoader.add('logo_png', 'res/logo.png');
			CLoader.add('bar_bottom_png', 'res/bar_bottom.png');
			CLoader.add('bar_top_png', 'res/bar_top.png');
			CLoader.add('bar_png', 'res/bar.png');

			CLoader.addListener(function () {
				CLoaderScene.add('frankfurter_medium_26_png', 'res/frankfurter_medium_26.png');
				CLoaderScene.add('frankfurter_medium_26_fnt', 'res/frankfurter_medium_26.fnt');
				CLoaderScene.add('frankfurter_medium_34_png', 'res/frankfurter_medium_34.png');
				CLoaderScene.add('frankfurter_medium_34_fnt', 'res/frankfurter_medium_34.fnt');
				CLoaderScene.add('frankfurter_medium_40_png', 'res/frankfurter_medium_40.png');
				CLoaderScene.add('frankfurter_medium_40_fnt', 'res/frankfurter_medium_40.fnt');
				CLoaderScene.add('frankfurter_medium_42_png', 'res/frankfurter_medium_42.png');
				CLoaderScene.add('frankfurter_medium_42_fnt', 'res/frankfurter_medium_42.fnt');
				CLoaderScene.add('frankfurter_medium_48_png', 'res/frankfurter_medium_48.png');
				CLoaderScene.add('frankfurter_medium_48_fnt', 'res/frankfurter_medium_48.fnt');

				CLoaderScene.add('landscape_background_jpg', 'res/landscape_background.jpg');
				CLoaderScene.add('portrait_background_jpg', 'res/portrait_background.jpg');

				CLoaderScene.add('dialog_background_png', 'res/dialog_background.png');

				var ranks = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"];
				var colors = ['Club', 'Diamond', 'Heart', 'Spade'];

				for (var i in colors) {
					var color = colors[i];
					for (var j in ranks) {
						var rank = ranks[j];
						var name = rank + color;
						CLoaderScene.add(name + '_png', 'res/' + name + '.png');

					}
				}

				CLoaderScene.add('card_back_png', 'res/card_back.png');
				CLoaderScene.add('joker_png', 'res/joker.png');

				CLoaderScene.add('tower_left_png', 'res/tower_left.png');
				CLoaderScene.add('tower_middle_png', 'res/tower_middle.png');
				CLoaderScene.add('tower_right_png', 'res/tower_right.png');

				CLoaderScene.add('logo_png', 'res/logo.png');

				CLoaderScene.add('icon_highscore_png', 'res/icon_highscore.png');
				CLoaderScene.add('icon_score_png', 'res/icon_score.png');
				CLoaderScene.add('icon_clock_png', 'res/icon_clock.png');
				CLoaderScene.add('icon_cards_run_png', 'res/icon_cards_run.png');
				CLoaderScene.add('icon_level_round_png', 'res/icon_level_round.png');

				CLoaderScene.add('label_cup_highscrore_png', 'res/label_cup_highscrore.png');
				CLoaderScene.add('star_png', 'res/star.png');

				CLoaderScene.add('score_time_icon_png', 'res/score_time_icon.png');
				CLoaderScene.add('score_joker_icon_png', 'res/score_joker_icon.png');
				CLoaderScene.add('score_card_icon_png', 'res/score_card_icon.png');
				CLoaderScene.add('score_level_round_icon_png', 'res/score_level_round_icon.png');
				CLoaderScene.add('score_cup_icon_png', 'res/score_cup_icon.png');

				CLoaderScene.add('title_options_png', 'res/title_options.png');
				CLoaderScene.add('title_score_cup_png', 'res/title_score_cup.png');

				CLoaderScene.add('button_moregames_png', 'res/button_moregames.png');
				CLoaderScene.add('button_play_png', 'res/button_play.png');
				CLoaderScene.add('button_play_pushed_png', 'res/button_play_pushed.png');
				CLoaderScene.add('button_sound_on_png', 'res/button_sound_on.png');
				CLoaderScene.add('button_sound_on_pushed_png', 'res/button_sound_on_pushed.png');
				CLoaderScene.add('button_sound_off_png', 'res/button_sound_off.png');
				CLoaderScene.add('button_sound_off_pushed_png', 'res/button_sound_off_pushed.png');
				CLoaderScene.add('button_home_png', 'res/button_home.png');
				CLoaderScene.add('button_home_pushed_png', 'res/button_home_pushed.png');
				CLoaderScene.add('button_back_png', 'res/button_back.png');
				CLoaderScene.add('button_back_pushed_png', 'res/button_back_pushed.png');
				CLoaderScene.add('button_next_png', 'res/button_next.png');
				CLoaderScene.add('button_next_pushed_png', 'res/button_next_pushed.png');
				CLoaderScene.add('button_options_png', 'res/button_options.png');
				CLoaderScene.add('button_options_pushed_png', 'res/button_options_pushed.png');
				CLoaderScene.add('button_restart_undo_png', 'res/button_restart_undo.png');
				CLoaderScene.add('button_restart_undo_pushed_png', 'res/button_restart_undo_pushed.png');
				CLoaderScene.add('button_highscore_png', 'res/button_highscore.png');
				CLoaderScene.add('button_highscore_pushed_png', 'res/button_highscore_pushed.png');

				CLoaderScene.add('button_mp3', 'res/button.mp3');
				CLoaderScene.add('card_place_mp3', 'res/card_place.mp3');
				CLoaderScene.add('card_mp3', 'res/card.mp3');
				CLoaderScene.add('error_mp3', 'res/error.mp3');
				CLoaderScene.add('lose_mp3', 'res/lose.mp3');
				CLoaderScene.add('music_mp3', 'res/music.mp3');
				CLoaderScene.add('new_game_mp3', 'res/new_game.mp3');
				CLoaderScene.add('new_high_score_mp3', 'res/new_high_score.mp3');
				CLoaderScene.add('part_opened_mp3', 'res/part_opened.mp3');
				CLoaderScene.add('pick_card_mp3', 'res/pick_card.mp3');
				CLoaderScene.add('place_card_mp3', 'res/place_card.mp3');
				CLoaderScene.add('score_add_mp3', 'res/score_add.mp3');
				CLoaderScene.add('victory_mp3', 'res/victory.mp3');
				CLoaderScene.add('window_mp3', 'res/window.mp3');

				CLoaderScene.addListener(this.onAssetsLoaded, this);
				CLoaderScene.start();
			}, this);

			CLoader.start();

		}, this);
	},
	setupAPI: function () {
		Publisher.init();
		window.famobi_onPauseRequested = function () {
			Cup.pause();
		}
		window.famobi_onResumeRequested = function () {
			Cup.resume();
		}
	},
	setupLanguage: function () {
		this.setLanguage('en');
	},
	setupCookie: function () {
		Cookie.load();
	},
	setupSound: function functionName() {
		var state = Cookie.get(COOKIE_MUTE) > 0 ? true : false;

		CSound.muteMusic(state);
		CSound.muteSound(state);
	},
	setupBackground: function () {
		this.background = new cc.Sprite();
		this.background.setAnchorPoint(0.5, 0.5);
		Cup.display.addChild(this.background, -1);
	},
	setupScreens: function () {
		Cup.display.addScreen(SCREEN_MENU, new MenuScreen);
		Cup.display.addScreen(SCREEN_GAME, new GameScreen);

		Cup.display.addScreen(DIALOG_OPTIONS, new OptionsDialog);
		Cup.display.addScreen(DIALOG_SCORE, new ScoreDialog);
		Cup.display.addScreen(DIALOG_GAMEOVER, new GameOverDialog);

		Cup.display.showScreen(SCREEN_MENU);
	},
	setupUI: function () {},
	start: function () {
		this.setupAPI();
		this.setupLanguage();
		this.setupCookie();
		this.setupSound();
		this.setupBackground();
		this.setupUI();
		this.setupScreens();

		Cup.onResizeCallback = this.onResize.bind(this);
		Cup.resize();
	},
	onAssetsLoaded: function (e) {
		Cup.start(this.start, this);
	},
	onResize: function () {
		var winSize = cc.director.getWinSize();

		var landscape = Cup.landscape;
		if (landscape) {
			this.background.setTexture(CLoaderScene.get('landscape_background_jpg'));
		} else {
			this.background.setTexture(CLoaderScene.get('portrait_background_jpg'));
		}
		this.background.setPosition(winSize.width / 2, winSize.height / 2);
	},
	getBackground: function () {
		return this.background;
	}
};
