// Signals
var SIGNAL_BUTTON_TAP = new CSignal();
var SIGNAL_LEVEL_START = new CSignal();
var SIGNAL_COIN_CHANGE = new CSignal();
var SIGNAL_LIVE_CNAHGE = new CSignal();
var SIGNAL_FAILED = new CSignal();

// Branding
var MORE_GAMES_URL = 'https://moregamesurl';

// Cookie
var COOKIE_STATUS = 'status';
var COOKIE_SCORE = 'score';
var COOKIE_MUTE = 'mute';

// Localization
var LANGUAGE = 'en';

var LANG_TOUCH_SCREEN_TO_START = 'touchScreenToStart';
var LANG_TOTAL_LABEL = 'totalLabel';

var IS_MUSIC_PLAYING = false;

// Font
var LABEL_STROKE_COLOR = cc.color('#86162D');
var LABEL_STROKE_SIZE = 6;
var LABEL_FONT_NAME = null; //'Frankfurter-Medium';

// Display
var SCREEN_MENU = 'menuScreen';
var SCREEN_GAME = 'gameScreen';
var DIALOG_OPTIONS = 'optionsDialog';
var DIALOG_SCORE = 'scoreDialog';
var DIALOG_GAMEOVER = 'gameOverDialog';
