var SceneLoader = cc.Scene.extend({
    init: function() {
        this.container = new cc.Node();
        this.addChild(this.container);

        this.logo = new cc.Sprite(CLoader.get('logo_png'));
        this.logo.setAnchorPoint(0.5, 0.5);
        this.container.addChild(this.logo);

        var barPositionY = -230;

        this.bar = new cc.Sprite(CLoader.get('bar_png'));
        this.bar.setPosition(-this.bar.getContentSize().width / 2, barPositionY);
        this.bar.setScale(0, 1);
        this.bar.setAnchorPoint(0, 0.5);

        this.barBottom = new cc.Sprite(CLoader.get('bar_bottom_png'));
        this.barBottom.setAnchorPoint(0.5, 0.5);
        this.barBottom.setPosition(0, barPositionY);

        this.barTop = new cc.Sprite(CLoader.get('bar_top_png'));
        this.barTop.setAnchorPoint(0.5, 0.5);
        this.barTop.setPosition(0, barPositionY - 2);

        this.container.addChild(this.barBottom);
        this.container.addChild(this.bar);
        this.container.addChild(this.barTop);

        this.label = new cc.LabelTTF("0%", "Arial", 22);
        this.label.setAnchorPoint(0.5, 0.5);
        this.label.setPosition(0, barPositionY - 2);
        this.label.setFontName(LABEL_FONT_NAME);
        this.label.setColor(cc.color.BLACK);

        this.container.addChild(this.label);
        this.resize();
    },
    onEnter: function() {
        this._super();
        this.schedule(this.onStartLoading, 0.3);
    },
    onExit: function() {
        this._super();
        this.label.setString("");
    },
    initWithUrls: function(urls, callback, target) {
        if (cc.isString(urls)) {
            urls = [urls];
        }
        this.urls = urls || [];
        this.callback = callback;
        this.target = target;
    },
    onStartLoading: function() {
        this.unschedule(this.onStartLoading);

        cc.loader.load(this.urls,
            function(result, count, loadedCount) {
                var percent = (loadedCount / count * 100) | 0;
                percent = Math.min(percent, 100);

                this.bar.setScale(percent / 100, 1);
                this.label.setString(percent + "%");

            }.bind(this),
            function() {
                this.bar.setScale(1);
                this.label.setString("100%");

                if (this.callback) {
                    this.callback.call(this.target);
                }
            }.bind(this));
    },
    resize: function() {
        var winSize = cc.director.getWinSize();
        if (this.container) {
            this.container.setPosition(winSize.width / 2, winSize.height / 2);
        }
    },

    preload: function(urls, callback, target) {
        this.init();
        this.initWithUrls(urls, callback, target);
        cc.director.runScene(this);
    }
});