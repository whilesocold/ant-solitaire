var Cookie = {
    reset: function() {
        CCookie.clear();

        if (!this.hashMap) {
            this.hashMap = [];
        }

        this.set(COOKIE_STATUS, 1);
        this.set(COOKIE_SCORE, 0);
        this.set(COOKIE_MUTE, 0);

        this.save();
    },
    load: function() {
        if (!this.hashMap) {
            this.hashMap = [];
        }
        if (!CCookie.get(COOKIE_STATUS)) {
            this.reset();
        } else {
            this.set(COOKIE_STATUS);
            this.set(COOKIE_SCORE);
            this.set(COOKIE_MUTE);

            for (var key in this.hashMap) {
                var value = CCookie.get(key);
                if (value) {
                    this.set(key, parseInt(value));
                }
            }
        }
    },
    save: function() {
        for (var key in this.hashMap) {
            var value = this.get(key);
            CCookie.set(key, value);
        }
    },
    set: function(key, value) {
        this.hashMap[key] = value;
    },
    get: function(key) {
        return this.hashMap[key];
    }
};