var LabelWithSprite = cc.Node.extend({
   ctor: function(spriteTexture, labelString) {
      this._super();

      this.setCascadeOpacityEnabled(true);

      this.sprite = new cc.Sprite(spriteTexture);
      this.sprite.setAnchorPoint(0, 0.5);
      this.sprite.setCascadeOpacityEnabled(true);
      this.addChild(this.sprite);

      this.label = new cc.LabelBMFont(labelString, CLoaderScene.get('frankfurter_medium_42_fnt'));
      this.label.setAnchorPoint(0, 0.5);
      this.addChild(this.label);

      this.setOffset(25);
   },
   setOffset: function(value) {
      this.offset = value;
      this.update();
   },
   getOffset: function() {
      return this.offset;
   },
   update: function() {
      var spriteContentSize = this.sprite.getContentSize();
      var labelContentSize = this.label.getContentSize();

      this.label.setPosition(
         this.sprite.x + spriteContentSize.width + this.offset,
         this.sprite.y);

      var contentSize = cc.size(spriteContentSize.width + labelContentSize.width + this.offset, Math.max(spriteContentSize.height, labelContentSize.height));
      this.setContentSize(contentSize);
   }
});