﻿ //Publisher Famobi API Implementation
var Publisher = Publisher || {};

//parameter to make the daily challenge optionally replayable
Publisher.dailyReplayable = false;

//
Publisher.create = function(url) {
    var fgJS = document.createElement('script');
    var firstJS = document.getElementsByTagName('script')[0];
    fgJS.src = url + encodeURIComponent(document.location.href);
    firstJS.parentNode.insertBefore(fgJS, firstJS);
};

//
Publisher.init = function() {
    //
    window.famobi = window.famobi ? window.famobi : {};
    window.famobi.localStorage = window.famobi.localStorage ? window.famobi.localStorage : window.localStorage;
    window.famobi.sessionStorage = window.famobi.sessionStorage ? window.famobi.sessionStorage : window.sessionStorage;

    //score
    Publisher.submitHighscore = function(level, score) {
        try {
            window.famobi.submitHighscore(level, score);
        } catch (e) {}
    };

    Publisher.showHighscore = function() {
        try {
            window.famobi.showHighscore();
        } catch (e) {}
    };

    //ad
    Publisher.showAd = function() {
        try {
            window.famobi.showAd();
        } catch (e) {}
    };

    //more games
    Publisher.moreGamesLink = function() {
        try {
            window.famobi.moreGamesLink();
        } catch (e) {}
    };
};

//
Publisher.create('/html5games/gameapi/v1.js?e=');