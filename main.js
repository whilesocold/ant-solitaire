window.onload = function() {
    cc.game.onStart = function() {
        setTimeout(function() {
            window.scrollTo(0, 1);
        }, 10);
        setTimeout(function() {
            App.onStart();
        }, 100);
    };
    cc.game.run();
};